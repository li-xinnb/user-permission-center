package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper extends CommonMapper<Role> {

    List<Role> selectRoleByUserid (Long userId);

    /**
     * @Description 角色通用查询
     * @author Gayrot
     * @date 2023/6/15 9:48
     * @param role
     * @return List<Role>
     */
    List<Role> commonQuery(Role role);


    /**
     * @Description 根据id 使用多表链接查询查找role对象
     * @author Gayrot
     * @date 2023/6/15 9:47
     * @param id
     * @return Role 带有 公司 机构  客户等字段的 role 对象
     */
    Role selectById(Long id);

    /**
     * 查询表中的公司
     * @param
     * @return 带有 公司 机构  客户等字段的 role 对象
     */
    List<Node> selectCompany();

    /**
     * 判断用户名是否已经存在
     * @param role
     * @return
     */
    int haveRole(Role role);

    /**
     * @Description 根据角色Id查找角色，用于编辑对话框展示
     * @author Gayrot
     * @date 2023/6/15 9:48
     * @param userId
     * @return List<Role>
     */
    List<Role> selectRolesByUserId(Long userId);

    /**
     * @Description 通过编号查找角色，用于角色更改中的编号重复判定
     * @author Gayrot
     * @date 2023/6/12 15:40
     * @param role
     * @return Role
     */
    Role getRoleByCode(Role role);

}