package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Administrative;
import com.sd365.permission.centre.pojo.query.AdministrativeQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:32
 */

@Mapper
public interface AdministrativeMapper extends CommonMapper<Administrative> {

    /**
     * @Description 通过id查询
     * @author Gayrot
     * @date 2023/6/12 10:32
     * @param id
     * @return Administrative
     */
    Administrative selectById(@Param("id") Long id);

    /**
     * @Description 模糊查询
     * @author Gayrot
     * @date 2023/6/12 10:33
     * @param administrativeQuery
     * @return List<Administrative>
     */
    List<Administrative> commonQuery(AdministrativeQuery administrativeQuery);
}
