package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:27
 */

@ApiModel(value="com.sd365.permission.centre.entity.Position")
@Table(name = "basic_position")
public class Position extends TenantBaseEntity {
    /**
     * 职位名称
     */
    @ApiModelProperty(value="name职位名称")
    private String name;

    /**
     * 职位编号
     */
    @ApiModelProperty(value="code职位编号")
    private String code;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;
    @Transient
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    /**
     * 所属机构
     */
    @Transient
    private Organization organization;
    /**
     * 职位名称
     */
    public String getName() {
        return name;
    }

    /**
     * 职位名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 职位编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 职位编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
