package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Message;
import com.sd365.permission.centre.pojo.query.MessageQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:47
 */

@Mapper
public interface MessageMapper extends CommonMapper<Message> {

    /**
     * @Description 通用查询带分页
     * @author Gayrot
     * @date 2023/6/12 10:47
     * @param messageQuery
     * @return List<Message>
     */
    List<Message> commonQuery(MessageQuery messageQuery);
}
