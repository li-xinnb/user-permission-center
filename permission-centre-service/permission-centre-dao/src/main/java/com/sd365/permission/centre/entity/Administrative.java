package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:22
 */

@ApiModel(value="com.sd365.permission.centre.entity.Administrative")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_administrative")
public class Administrative extends BaseEntity {

    /**
     * 区域名称
     */
    private String name;

    /**
     * 区域代码
     */
    private Long code;

    /**
     * 助记码
     */
    private String mnemonicCode;

    /**
     * 区域类型
     */
    private String areaType;

    /**
     * 排序号
     */
    private Long orderNum;

    /**
     * 备注
     */
    private String remark;
    /**
     * 组织ID
     */
    private Long orgId;

    /**
     * 租户ID
     */
    private Long tenantId;

    /**
     * 上级区域区域id
     */
    private Long parentId;
    /**
     * 修改者
     */
    private String modifier;
    /**
     * 创建者
     */
    private String creator;
    /**
     * 上级区域
     */
    private Administrative upAdministrative;

}
