package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Position;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface PositionMapper extends CommonMapper<Position> {
    List<Position> commonQuery(Position position);

    Position selectPositionById(Long id);

    Integer updateStatus(Position position);

    List<Position> queryPositionByStatus(PositionQuery positionQuery);
}

