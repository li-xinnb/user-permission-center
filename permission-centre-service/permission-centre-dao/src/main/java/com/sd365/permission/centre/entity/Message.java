package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:26
 */

@Data
@ApiModel(value = "com.sd365.permission.centre.entity.Message")
@Table(name = "basic_message")
@NoArgsConstructor
@AllArgsConstructor
public class Message extends BaseEntity {

    @ApiModelProperty(value = "messageType消息类型")
    private String messageType;

    @ApiModelProperty(value = "messageContent消息内容")
    private String messageContent;

    @ApiModelProperty(value = "pushTime推送间隔")
    private Long pushTime;

    @ApiModelProperty(value = "pushPath推送渠道")
    private String pushPath;

    @ApiModelProperty(value = "companyId公司id")
    private Long companyId;

    @ApiModelProperty(value = "modifier修改者")
    private String modifier;

    @ApiModelProperty(value = "creator创建者")
    private String creator;

    @ApiModelProperty(value = "orgId组织")
    private Long orgId;

    @ApiModelProperty(value = "tenantId租户")
    private Long tenantId;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Long getPushTime() {
        return pushTime;
    }

    public void setPushTime(Long pushTime) {
        this.pushTime = pushTime;
    }

    public String getPushPath() {
        return pushPath;
    }

    public void setPushPath(String pushPath) {
        this.pushPath = pushPath;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getModifier() {
        return modifier;
    }

    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
