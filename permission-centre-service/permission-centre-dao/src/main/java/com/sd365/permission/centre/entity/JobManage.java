package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:24
 */

@ApiModel(value = "com.sd365.permission.centre.entity.JobEntity")
@Table(name = "basic_job_manage")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobManage extends TenantBaseEntity {
    /**
     * 任务名称
     */
    @ApiModelProperty(value = "jobName任务名称")
    private String jobName;
    /**
     *任务分组
     */
    @ApiModelProperty(value = "jobGroup任务分组")
    private String jobGroup;
    /**
     *任务描述
     */
    @ApiModelProperty(value = "description任务描述")
    private String description;
    /**
     *调用字符串
     */
    @ApiModelProperty(value = "targetPath调用字符串")
    private String targetPath;
    /**
     *cron表达式
     */
    @ApiModelProperty(value = "cron cron表达式")
    private String cron;
    /**
     *计划错误策略，1错过计划时等待本次完成后立即执行一次，0本次执行时间根据上次结束时间重新计算（时间间隔方式）
     */
    @ApiModelProperty(value = "errorTactics计划错误策略，1错过计划时等待本次完成后立即执行一次，0本次执行时间根据上次结束时间重新计算（时间间隔方式）")
    private byte errorTactics;
    /**
     *是否执行并发，1表示执行，0表示不执行
     */
    @ApiModelProperty(value = "concurrentState是否执行并发，1表示执行，0表示不执行")
    private byte concurrentState;
    /**
     *其他信息
     */
    @ApiModelProperty(value = "otherInfo其他信息")
    private String otherInfo;

    @ApiModelProperty(value = "tenantId租户id")
    private Long tenantId;

    @ApiModelProperty(value="orgId组织")
    private Long orgId;

    @ApiModelProperty(value="companyId")
    private Long companyId;

    @ApiModelProperty(value = "creator")
    private String creator;

    @ApiModelProperty(value = "createdTime")
    private Date createdTime;

    @ApiModelProperty(value="createdBy")
    private Long createdBy;
    @ApiModelProperty(value="updatedBy")
    private Long updatedBy;

    @ApiModelProperty(value = "updatedTime")
    private Date updatedTime;
}
