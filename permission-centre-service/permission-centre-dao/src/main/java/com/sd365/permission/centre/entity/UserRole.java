package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:31
 */

@Data
@ApiModel(value="com.sd365.permission.centre.entity.UserRole")
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_user_role")
public class UserRole extends TenantBaseEntity {
    /**
     * 用户ID
     */
    @ApiModelProperty(value="userId用户ID")
    private Long userId;

    /**
     * 角色ID
     */
    @ApiModelProperty(value="roleId角色ID")
    private Long roleId;

    /**
     * 用户ID
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * 用户ID
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 角色ID
     */
    public Long getRoleId() {
        return roleId;
    }

    /**
     * 角色ID
     */
    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}