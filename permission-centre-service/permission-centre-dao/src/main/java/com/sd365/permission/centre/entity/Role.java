package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:28
 */

@ApiModel(value="com.sd365.permission.centre.entity.Role")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_role")
public class Role extends TenantBaseEntity {
    /**
     * 角色名
     */
    @ApiModelProperty(value="name角色名")
    private String name;

    /**
     * 角色代码
     */
    @ApiModelProperty(value="code角色代码")
    private String code;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 公司
     */
    @ApiModelProperty(value="company")
    private Company company;

    /**
     * 机构
     */
    @ApiModelProperty(value="organization")
    private Organization organization;

    /**
     * 租户
     */

    @ApiModelProperty(value="tenant")
    private Tenant tenant;
}