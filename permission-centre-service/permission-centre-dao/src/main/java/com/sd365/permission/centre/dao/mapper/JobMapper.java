package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.JobManage;
import com.sd365.permission.centre.pojo.query.JobQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:45
 */

@Mapper
public interface JobMapper extends CommonMapper<JobManage> {

    /**
     * @Description 通用分页查询
     * @author Gayrot
     * @date 2023/6/12 10:45
     * @param jobQuery
     * @return List<JobManage>
     */
    List<JobManage> commonQueryJob(JobQuery jobQuery);

    /**
     * @Description 暂停作业
     * @author Gayrot
     * @date 2023/6/12 10:45
     * @param id
     */
    void pauseJobStatus(Long id);

    /**
     * @Description 重置作业状态
     * @author Gayrot
     * @date 2023/6/12 10:46
     * @param id
     */
    void resumeJobStatus(Long id);

    /**
     * @Description 添加作业
     * @author Gayrot
     * @date 2023/6/12 10:46
     * @param job
     * @return int
     */
    int insertJob(JobManage job);

    /**
     * @Description 更新作业
     * @author Gayrot
     * @date 2023/6/12 10:46
     * @param job
     * @return int
     */
    int updateJob(JobManage job);

}
