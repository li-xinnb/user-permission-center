package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.RoleCompany;
import org.apache.ibatis.annotations.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

@Mapper
public interface RoleCompanyMapper extends CommonMapper<RoleCompany>, InsertListMapper<RoleCompany> {
}
