package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.pojo.dto.DepartmentDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:40
 */

@Mapper
public interface DepartmentMapper extends CommonMapper<Department> {

    /**
     * @Description 通用查询带分页
     * @author Gayrot
     * @date 2023/6/12 10:40
     * @param departmentQuery
     * @return List<Department>
     */
    List<Department> commonQueryDepartment(DepartmentQuery departmentQuery);

    /**
     * @Description 批量查询公司下的部门
     * @author Gayrot
     * @date 2023/6/12 10:41
     * @param companyList
     * @return List<Department>
     */
    List<Department> selectByCompanyList(ArrayList<Company> companyList);

    /**
     * @Description 根据一级部门id查询下一级部门
     * @author Gayrot
     * @date 2023/6/12 10:41
     * @param id
     * @return List<Department>
     */
    List<Department> selectByParentId(Long id);

    /**
     * @Description 根据公司id查询一级部门
     * @author Gayrot
     * @date 2023/6/12 10:41
     * @param id
     * @return List<Department>
     */
    List<Department> selectByCompanyId(Long id);

    /**
     * @Description 添加部门
     * @author Gayrot
     * @date 2023/6/12 10:41
     * @param department
     * @return int
     */
    //int insertDepartment(Department department);

    /**
     * @Description 修改部门状态（启停）
     * @author Gayrot
     * @date 2024/3/29 15:57
     * @param departmentDTO
     * @return int
     */

    int updateDepartmentStatus(DepartmentDTO departmentDTO);

    int getSonDepartmentCountById(DepartmentDTO departmentDTO);
}