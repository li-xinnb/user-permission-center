package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.pojo.query.CompanyQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:38
 */

@Mapper
public interface CompanyMapper extends CommonMapper<Company> {

    /**
     * @Description 分页查询，通常可以模糊查询
     * @author Gayrot
     * @date 2023/6/12 10:38
     * @param companyQuery
     * @return List<Company>
     */
    List<Company> commonQuery(CompanyQuery companyQuery);

    /**
     * @Description 插入一条数据
     * @author Gayrot
     * @date 2023/6/12 10:39
     * @param company
     * @return int
     */
    int insertCompany(Company company);

    /**
     * @Description 更新数据
     * @author Gayrot
     * @date 2023/6/12 10:39
     * @param company
     * @return int
     */
    int updateCompany(Company company);

    /**
     * @Description 通过id查询数据
     * @author Gayrot
     * @date 2023/6/12 10:39
     * @param id
     * @return Company
     */
    Company selectById(Long id);

    /**
     * @Description 获取当前机构下的所有公司
     * @author Gayrot
     * @date 2023/6/12 10:40
     * @param orgId
     * @return List<Company>
     */
    List<Company> findAllByOrgId(Long orgId);
}