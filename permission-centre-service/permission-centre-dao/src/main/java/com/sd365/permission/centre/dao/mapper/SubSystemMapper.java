package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.SubSystem;
import com.sd365.permission.centre.entity.Tenant;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SubSystemMapper extends CommonMapper<SubSystem> {

    /**
     *  subSystem的id 修改其信息
     * @param subSystem  subSystem 的实体数据
     * @return 修改返回条数1为成功，0为失败
     */
    int updateSubSystem(SubSystem subSystem);

    /**
     * 每个Mapper都可以定义属于自己通用查询
     * @param subSystem 查询对象一般是entity
     * @return 查询到的列表
     */
    List<SubSystem> commonQuery(SubSystem subSystem);

    /**
     *  根据id 查找对应的SubSystem对象
     * @param id  SubSystem 的 id
     * @return SubSystem对象
     */
    SubSystem selectById(Long id);

    /**
     *  根据name 查找对应的SubSystem对象
     * @param id  SubSystem 的 name
     * @return SubSystem对象
     */
    List<SubSystem> selectByTenantId(Long id);

    List<Tenant> tenantQuery();
}
