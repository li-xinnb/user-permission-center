package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:30
 */

@ApiModel(value="com.sd365.permission.centre.entity.User")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_user")
public class User extends TenantBaseEntity {
    /**
     * 角色用户所拥有的角色 可以通过 ResultMap <collection 指定子查询获取
     */
    @Transient
    private List<Role> roleList;

    /**
     * 角色用户所拥有的部门 可以通过 ResultMap <assocation 指定子查询获取
     * 你也考可以考虑分开查询 或者 不带该字段
     */
    @Transient
    private Department department;

    /**
     * 说明同上
     */
    @Transient
    private Position position;

    /**
     * 公司和组织机构以及租户也可以采用以上方式
     */

    /**
     * 工号
     */
    @ApiModelProperty(value="code工号")
    private String code;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 名字
     */
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 头像
     */
    @ApiModelProperty(value="profilePicture头像")
    private String profilePicture;

    /**
     * 性别
     */
    @ApiModelProperty(value="sex性别")
    private Byte sex;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    private Date birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 其他联系
     */
    @ApiModelProperty(value="other其他联系")
    private String other;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 部门ID
     */
    @ApiModelProperty(value="departmentId部门ID")
    private Long departmentId;

    /**
     * 职位ID  注意这个不等同于角色实际就是岗位
     */
    @ApiModelProperty(value="positionId职位ID  注意这个不等同于角色实际就是岗位")
    private Long positionId;

    /**
     * 公司
     */
    @Transient
    private Company company;

    /**
     * 组织
     */
    @Transient
    private Organization organization;


    /**
     * 租户ID  注意这个不等同于角色实际就是岗位
     */
    @ApiModelProperty(value="tenantId租户ID  租户ID")
    private Long tenantId;

}