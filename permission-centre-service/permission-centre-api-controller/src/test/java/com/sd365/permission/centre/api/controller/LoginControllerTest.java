/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName LoginControllerTest.java
 * @Author Administrator
 * @Date 2022-10-13  11:21
 * @Description 文件对提供网关认证服务调用的auth接口做单元测试
 * History:
 * <author> Administrator
 * <time> 2022-10-13  11:21
 * <version> 1.0.0
 * <desc>该文件主要对用户登录的 认证，角色信息获取，登录接口的测试用例进行要求
 *  * 验收程序通过 功能测试+单元测试的执行结果综合评定开发者的开发成果
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.permission.centre.dao.mapper.UserMapper;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.vo.LoginUserVO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableCaching
/**
 * @Class LoginControllerTest
 * @Description 针对控制器接口的单元测试类，选择了部分接口进行单元测试，开发人员必须按照模板
 *  * 完成单元测试，后续代码审查中重点审查单元测试是否能达到模板要求
 * @Author Administrator
 * @Date 2022-10-13  11:23
 * @version 1.0.0
 */
public class LoginControllerTest {
    /**
     * 注入测试类
     */
    @Autowired
    private LoginController loginController;

    @Autowired
    private UserMapper userMapper;
    /**
     * @Description: 测试前端发起登录对用户的身份检查以及返回基本用户数据的功能
     * @Author: Administrator
     * @DATE: 2022-10-13  11:25
     */
    @Test
    public void auth() {
        /**
         *  正用例
         * */
        LoginUserVO userVO = null;
        //测试步骤1
         // 传入参数 传入正确的账号 密码  租户 （限制条件 挑选 用户拥有角色）
          // 返回数据符合预期 UserVO的code 为200 其他数据也正确
        userVO = loginController.login("100002","sd365sd365","BOSSSOFT");
        Assert.assertEquals(200,userVO.getCode());
        /**
         * 反用例
         */
        //测试步骤2
        // 传入参数 传入正确的账号 租户  错误的密码   UserVO的code 为401
        userVO = loginController.login("100002","sd365sd3657","BOSSSOFT");
        Assert.assertEquals(401,userVO.getCode());

        //测试步骤3
        // 传入参数 传入正确的账号 密码 和错误的租户账号 UserVO的code 为401
        userVO = loginController.login("100002","sd365sd3657","**BOSSSOFT");
        Assert.assertEquals(401,userVO.getCode());

        //测试步骤4
        // 传入参数 比较字段为空 ，校验不通过抛出异常
        try{
            loginController.login(null,null,null);
        }catch (Exception e){
            e.printStackTrace();
        }

        // 测试步骤5
        // 传入参数 传入正确的账号 密码  租户 （限制条件 用户无角色） UserVO的code 为200 但是角色列表为空
        userVO = loginController.login("1007","a123456","BOSSSOFT");
        Assert.assertEquals(200,userVO.getCode());
        Assert.assertNull(userVO.getRoleIds());

        // 测试步骤6
        // 传入参数  账号，密码存在，租户为空 校验不通过抛出异常
        try {
            loginController.login("100002", "sd365sd365", null);
        }catch (Exception e){
            e.printStackTrace();
        }
        // 测试步骤7
        // 传入参数  账号【为空】，密码存在，租户存在 校验不通过抛出异常
        try {
            loginController.login(null, "sd365sd365", "BOSSSOFT");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Test
    public void info() {

        LoginUserVO userVO = null;
        /**
         *  正用例
         * */
        //测试步骤1
        // 传入参数 正确的账号和租户账号 返回正确的角色信息以及角色权限列表
        userVO = loginController.info("100002","BOSSSOFT");
        Assert.assertNotNull(userVO.getRoleIds());
        Assert.assertNotNull(userVO.data.getResourceVO());

        /**
         * 反用例
         */
        //测试步骤2
        // 传入参数账号错误，租户正确  返回信息空对象
        userVO = loginController.info("0011","BOSSSOFT");
        Assert.assertNull(userVO);

        //测试步骤3
        // 传入n账号正确，租户正确，但是无角色 ， 返回 UserVO 角色和资源列表为空对象
        userVO = loginController.info("1007","BOSSSOFT");
        Assert.assertEquals(0,userVO.getRoleIds().size());
        Assert.assertNull(userVO.data.getResourceVO());

        //测试步骤
        // 传入账号正确，租户正确 但是角色无权限 返回 UserVO  角色列表有数据 资源列表为空
        userVO = loginController.info("1008","BOSSSOFT");
        Assert.assertNotNull(userVO.getRoleIds());
        Assert.assertEquals(0,userVO.data.getResourceVO().size());
    }

    @Test
    public void logout() {
        Long id = 1669668914801541120L;
        BaseContextHolder.setLoginUserId(id);
        /**
         *  正用例
         * */
        //测试步骤1
        // 传入参数 用户id  符合期望 用户状态边下线，redis的token被清除
        loginController.logout();
        User user = userMapper.selectByPrimaryKey(id);
        Assert.assertEquals(1,Integer.parseInt(String.valueOf(user.getStatus())));
        /**
         * 反用例
         */
        //测试步骤2
        // 传入参数的用户id为空 校验不通过抛出异常
        BaseContextHolder.setLoginUserId(null);
        try{
            loginController.logout();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}