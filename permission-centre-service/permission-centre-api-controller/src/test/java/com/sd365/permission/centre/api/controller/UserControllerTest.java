/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName UserControllerTest.java
 * @Author Administrator
 * @Date 2022-9-28  21:41
 * @Description 该文件对培训项目中用户管理主要接口（增删改查、角色分配）的单元测试标准做规范，基于此对培训项目的验收进行批评
 * History:
 * <author> Administrator
 * <time> 2022-9-28  21:41
 * <version> 1.0.0
 * <desc> 该文件对培训项目中用户管理主要接口的单元测试标准做规范，基于此对培训项目的验收进行批评
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.StringUtil;
import com.sd365.permission.centre.pojo.dto.UserCentreDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @Class UserControllerTest
 * @Description  按验收要求对资源管理部分接口进行单元测试覆盖
 * @Author Administrator
 * @Date 2022-9-28  21:41
 * @version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class UserControllerTest {


    @Autowired
    private UserController userController;

    void setBaseContextHolderInfo(){
        BaseContextHolder.setCompanyId(1337960193258029056L);
        BaseContextHolder.setOrgId(1337955940401545216L);
        BaseContextHolder.setTanentId(1337940702788714496L);
    }


    /**
     * @Description: 单元测试控制器add方法
     * @Author: Administrator
     * @DATE: 2022-9-28  21:41
     * @param: 
     * @return: 
     */
    @Test
    void add() {
        setBaseContextHolderInfo();
        UserCentreDTO userCentreDTO = new UserCentreDTO();
        userCentreDTO.setId(1671049970494603264L);
        userCentreDTO.setCode("1008");
        userCentreDTO.setName("单元测试用户x");
        userCentreDTO.setEmail("1@qq.com");
        userCentreDTO.setTel("12345678902");
        userCentreDTO.setPassword("");
        try {
            Boolean result = userController.modifyUserInfoForBm(userCentreDTO);
            Assert.assertTrue(result);
        }catch (Exception e){
            e.printStackTrace();
        }

        /**
         *  正用例
         * */
        //测试步骤1
              // 传入参数 包含界面必须输入的字段，非必须输入字段为空  是否合法预期结果
        //测试步骤2
            // 传入参数  传入参数 包含界面必须输入的字段，非必须输入字段比为空   是否合法预期结果
        /**
         * 反用例
         */
        //测试步骤4
              // 传入参数  包含部分界面必须输入的字段，非必须输入字段为空  是否合法预期结果
        //测试步骤5
             // 传入参数  必须输入字段全部输入，工号重复，  抛出异常无法正常完成输入，提示工号不可以重复
        //测试步骤6
           // 传入参数  必须输入字段全部输入，名字重复， 输入成功
        // 测试步骤7
        // 传入参数  必须输入字段全部输入， 输入成功，取得密码判断正确md5加密
        // 测试步骤 8
           //传入空对象 被系统拦截抛出异常
    }
    /**
     * @Description: 单元测试控制器remove方法
     * @Author: Administrator
     * @DATE: 2022-9-28  21:41
     * @param: 
     * @return: 
     */
    @Test
    void remove() {
        /**
         *  正用例
         * */
        //测试步骤1
        // 传入参数 包含正确的删除比较字段       是否合法预期结果（用户角色信息被删除）
        /**
         * 反用例
         */
        //测试步骤2
        // 传入参数不存在的用户比较信息，是否合法的预期结果
        //测试步骤3
        // 传入null的参数信息  系统抛出异常
        //测试步骤4
        // 传入参数 比较字段为空 ，校验不通过抛出异常
    }
    /**
     * @Description: 单元测试控制器remove方法
     * @Author: Administrator
     * @DATE: 2022-9-28  21:41
     * @param: 
     * @return: 
     */
    @Test
    void modify() {
       // 测试步骤1
            // 传入UserDTO 必输字段不为空 ，非必输入字段也不为空，返回成功
        // 测试步骤2
             // 传入UserDTO 必输字段不为空 ，非必输入字段也为空，返回成功
        // 测试步骤3
            // 传入UserDTO 必输字段部分为空  校验不通过触发检查异常
             // 密码，工号，用户名，角色 进行适当组合测试
        // 测试步骤4
          // 传入UserDTO 必输字段部分不为空，工号重复  抛出异常提示工号重复

    }
    /**
     * @Description: 单元测试控制器方法modifyWithNewRole，该方法是用户管理模块的核心部分
     * @Author: Administrator
     * @DATE: 2022-9-28  21:41
     * @param: 
     * @return: 
     */
    @Test
    void modifyWithNewRole() {
        // 测试步骤1
             // 构建存在角色的用户对象，在角色列表增加1个新的角色，调用分配角色方法 ，预期结果新的用户增加了一个角色
        //测试步骤2
            // 构建存在角色的用户对象，角色列表只存1个新的角色，调用分配角色方法 预期结果 用户角色变成新的角色
        //测试步骤3
           // 构建存在角色的用户对象，角色列表存储0个角色对象，但是角色列表不是null，调用分配角色方法，触发异常提示用户至少一个角色
        //测试步骤4
           // 构建一个UserDTO（用户对象），角色列表为null，校验不通过触发异常
        // 测试步骤5
           // 构建一个UserDTO  用户id必须输入字段为null，校验不通过触发异常
        // 测试步骤6
           // 传入一个 null的UserDTO 对象，校验不通过触发异常
        // 测试步骤7
          // 传入一个用户对象(UserDTO) 其中角色列表包含引起异常的数据，预期结果用户分配角色事务被回滚
    }
    /**
     * @Description: 单元测试控制器类 batchDelete
     * @Author: Administrator
     * @DATE: 2022-9-28  21:42
     * @param: 
     * @return: 
     */
    @Test
    void batchDelete() {
        //测试步骤1
            //构建一个存在的用户数据传入方法 预期删除成功
        //测试步骤2
            // 构建2个存在的用户数据（放入数组或者list）调用测试方法，预期删除成功
        //测试步骤3
            // 构建 1个检查字段为空的数据放入列表，调用测试方法 检查不通过触发异常
        //测试步骤4
            // 传入null对象传入方法，检查不通过抛出异常
    }
}