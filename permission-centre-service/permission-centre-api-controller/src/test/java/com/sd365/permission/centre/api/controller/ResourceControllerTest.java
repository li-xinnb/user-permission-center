/**
 * Copyright (C), 2001-${YEAR}, www.bosssof.com.cn
 * @FileName ResourceControllerTest.java
 * @Author Administrator
 * @Date 2022-9-28  16:36
 * @Description 针对控制器层编写单元测试，因为是培训考核项目所以单元测试不覆盖所有的方法
 * 也不强调执行 语句逻辑覆盖，测试从实现功能测试，因为每个开发实现的语句逻辑可能存在差异，
 * 有余力的开发同学可以 强化语句覆盖和条件和判定的覆盖的用例
 * History:
 * <author> Administrator
 * <time> 2022-9-28  16:36
 * <version> 1.0.0
 * <desc> 验收项目资源管理的部分单元测试
 *
 */
package com.sd365.permission.centre.api.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;
/**
 * @Class ResourceControllerTest
 * @Description  按验收要求对资源管理部分接口进行单元测试覆盖
 * @Author Administrator
 * @Date 2022-9-28  16:00
 * @version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class ResourceControllerTest {
    /**
     * @Description: 测试查询功能点
     * @Author: Administrator
     * @DATE: 2022-9-28  16:00
     * @param:
     * @return:
     */
    @Test
    void commonQuery() {
        // 测试步骤1
             // 完成相等的资源查询  查询出符合记录数的列表 且分页信息正确
        // 测试步骤2
            // 完成模糊匹配的资源查询 查询出符合记录数的列表且分页信息正确
        // 测试步骤3
          //  内容为空查询出所有的资源信息且分页信息正确
    }

    /**
     * @Description: 左边功能树查询，前端在created钩子中调用该查询
     * @Author: Administrator
     * @DATE: 2022-9-28  16:00
     * @param:
     * @return:
     */
    @Test
    void queryResourceTree() {
        //测试步骤1
          // 传入空参数完成查询返回的所有的记录数
        //测试步骤2
            // 传入指定的子系统查询出符合该子系统的记录数

    }


    /**
     * @Description: 测试控制器层增加方法，前端弹出添加对话框输入数据点保存的时候调用后端接口
     * @Author: Administrator
     * @DATE: 2022-9-28  16:00
     * @param:
     * @return:
     */
    @Test
    void add() {
        /***
         * 开始正用例测试
         */
        // 测试步骤1
           // 传入所有的必填字段的DTO对象 返回成功值
        // 测试步骤2
           // 传入所有的必填和部分非必填的字段的DTO对象，返回成功
        /***
         * 开始反用例测试
         */
        // 测试步骤1
             // 传入部分的必填字段为空的DTO对象 返回失败值
        // 测试步骤2
             // 传入 编号 重复的 DTO  预期 添加不成功 抛出异常
        // 测试步骤3
             //  传入 子系统和 API 组合已经存在的DOT 预期添加不成功 抛出异常
        // 测试步骤4
            // 传入 子系统和 URL 组合重复的资源 预期添加不成功 抛出异常

    }


    /**
     * @Description: 测试控制器层批量删除方法，前端选择多条记录然后点击删除
     * @Author: Administrator
     * @DATE: 2022-9-28  16:00
     * @param:
     * @return:
     */
    @Test
    void batchDelete() {
        // 测试步骤1
            // 测试1条记录  返回符合预期的值并且查询到不到相应的数据
            // 测试2条记录  返回符合预期的值并且查询到不到相应的数据
        // 测试步骤2
             // 测试 传入id不匹配的记录 返回符合预期的值
        // 测试步骤3
    }


    /**
     * @Description: 测试修改功能点
     * @Author: Administrator
     * @DATE: 2022-9-28  16:00
     * @param:
     * @return:
     */
    @Test
    void modify() {
         /** 开始正用例测试 **/
        // 测试步骤1
        // 传入 一个DTO对象 必须填写参数不为空，修改其中某一些必填和非必填的，返回成功 并且通过查询方法查询到对方值符合预期，
        /** 开始反用例测试 **/
        // 测试步骤2
          // 测试 传入部分必填字段为空的DTO 系统抛出异常
        // 测试步骤3
           // 测试传入一个 资源编号和其他某一个资源重复的 系统触发异常修改失败不允许资源号同步
        // 测试步骤4
         // 测试传入一个 子系统和api 组合后和系统中的数据重复的 系统触发异常修改失败不允许资源号同步
        // 测试步骤5
        // 测试传入一个 子系统和URL 组合后和系统中的数据重复的 系统触发异常修改失败不
    }
}