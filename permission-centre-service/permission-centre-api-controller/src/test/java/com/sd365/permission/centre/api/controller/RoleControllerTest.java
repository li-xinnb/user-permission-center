/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName RoleControllerTest.java
 * @Author Administrator
 * @Date 2022-9-28  19:58
 * @Description 指定单元测试验收标准
 * History:
 * <author> Administrator
 * <time> 2022-9-28  19:58
 * <version> 1.0.0
 * <desc> 该文件主要对角色管理的 角色 增删改查 以及 角色分配资源和角色分配用户的测试用例进行要求
 * 验收程序通过 功能测试+单元测试的执行结果综合评定开发者的开发成果
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.RoleMapper;
import com.sd365.permission.centre.dao.mapper.RoleResourceMapper;
import com.sd365.permission.centre.dao.mapper.UserRoleMapper;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.RoleResource;
import com.sd365.permission.centre.entity.UserRole;
import com.sd365.permission.centre.pojo.dto.RoleDTO;
import com.sd365.permission.centre.pojo.dto.RoleResourceDTO;
import com.sd365.permission.centre.pojo.dto.UserRoleDTO;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.vo.RoleVO;
import lombok.val;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @Class RoleControllerTest
 * @Description 针对控制器接口的单元测试类，选择了部分接口进行单元测试，开发人员必须按照模板
 * 完成单元测试，后续代码审查中重点审查单元测试是否能达到模板要求
 * @Author Administrator
 * @Date 2022-9-28  20:00
 * @version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class RoleControllerTest {

    @Autowired
    private RoleController roleController;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private RoleResourceMapper roleResourceMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    void setBaseContextHolderInfo(){
        BaseContextHolder.setCompanyId(1337960193258029056L);
        BaseContextHolder.setOrgId(1337955940401545216L);
        BaseContextHolder.setTanentId(1337940702788714496L);
    }

    /**
     * @Description     为测试删除而存在的添加角色
     * @author Gayrot
     * @date 2023/6/16 11:31
     * @return Long
     */
    Long addRoleForTest(){
        setBaseContextHolderInfo();
        int prefix = (int) (Math.random() * 1000);
        RoleDTO roleDTO1 = new RoleDTO();
        roleDTO1.setCode("0" + prefix);
        roleDTO1.setName("岩石巨魔" + prefix);
        roleDTO1.setStatus(Byte.parseByte("0"));
        roleController.add(roleDTO1);
        return BaseContextHolder.get("TESTID",Long.class);
    }

    /**
     * @Description  为测试资源分配服务，获取分配资源后的条数
     * @author Gayrot
     * @date 2023/6/16 14:05
     * @param roleId
     * @return int
     */
    int getResourceNumber(Long roleId){
        Example example = new Example(RoleResource.class);
        example.createCriteria().andEqualTo("roleId",roleId);
        List<RoleResource> list = roleResourceMapper.selectByExample(example);
        return list.size();
    }

    /**
     * @Description  为测试用户分配服务，获取分配用户后的用户数量
     * @author Gayrot
     * @date 2023/6/16 15:11
     * @param roleId
     * @return int
     */
    int getUserNumber(Long roleId){
        Example example = new Example(UserRole.class);
        example.createCriteria().andEqualTo("roleId",roleId);
        List<UserRole> list = userRoleMapper.selectByExample(example);
        return list.size();
    }

    /**
     * @Description     测试控制器的add方法
     * @author Gayrot
     * @date 2023/6/16 11:11
     */
    @Test
    void add() {
        setBaseContextHolderInfo();
        // 测试步骤1
           // 测试必须输入项目完整的下角色添加是否成功
        RoleDTO roleDTO1 = new RoleDTO();
        int prefix = (int)(Math.random() * 1000);
        roleDTO1.setCode("000" + prefix);
        roleDTO1.setName("鹿首精" + prefix);
        roleDTO1.setStatus(Byte.parseByte("0"));
        try {
            Boolean addResultWithCompletePara = roleController.add(roleDTO1);
            Assert.assertTrue("添加成功", addResultWithCompletePara);
        }catch (Exception e){
            e.printStackTrace();
        }
        // 测试步骤2
           // 测试必输项目不完整的情况下 是否触发异常添加是啊比
        RoleDTO roleDTO2 = new RoleDTO();
        roleDTO2.setName("日间妖灵");
        try {
            Boolean addResultWithoutCompletePara = roleController.add(roleDTO2);
            Assert.assertFalse("添加失败", addResultWithoutCompletePara);
        }catch (Exception e){
            e.printStackTrace();
        }
        // 测试步骤3
           // 测试加入重复的角色编号的角色对象 系统是否会报错触发异常
        RoleDTO roleDTO3 = new RoleDTO();
        roleDTO3.setCode("000" + prefix);
        roleDTO3.setName("冰霜巨魔");
        try {
            Boolean addResultWithHasExistCode = roleController.add(roleDTO3);
            Assert.assertFalse("添加成功", addResultWithHasExistCode);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @Description     测试控制器的remove方法
     * @author Gayrot
     * @date 2023/6/16 11:11
     */
    @Test
    void remove() {
        //随机生成一个角色
        Long id = addRoleForTest();
        // 测试步骤1
           // 测试传入存在角色id的删除是否成功 包括角色的关联资源和用户的关系都被清除
        try {
            Boolean remove = roleController.remove(id, 0L);
            Assert.assertTrue(remove);
        }catch (Exception e){
            e.printStackTrace();
        }

        // 测试步骤2
           // 测试传入不存在角色id的删除是否失败
        try {
            roleController.remove(0L, 0L);
        }catch (Exception e){
            e.printStackTrace();
        }

        // 测试步骤3
           // 测试传入参数为空的时候是否触发异常
        try {
            roleController.remove(null, null);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @Description 测试控制器 batchRemove方法
     * @author Gayrot
     * @date 2023/6/16 12:37
     */
    @Test
    void batchRemove() {
        // 测试步骤1
           // 测试删除一条角色数据的时候是否成功
        Long id = addRoleForTest();
        RoleDTO roleDTO1 = new RoleDTO();
        roleDTO1.setId(id);
        roleDTO1.setCode("**");
        roleDTO1.setName("**");
        roleDTO1.setStatus(Byte.parseByte("0"));
        try {
            Boolean result1 = roleController.batchRemove(new RoleDTO[]{roleDTO1});
            Assert.assertTrue(result1);
        }catch (Exception e){
            e.printStackTrace();
        }

        // 测试步骤2
            // 测试删除2条角色数据的时候是否成功
        RoleDTO[] roleDTOS = new RoleDTO[2];
        for (int i = 0; i < 2; i++) {
            Long tId = addRoleForTest();
            RoleDTO roleDTO = new RoleDTO();
            roleDTO.setId(tId);
            roleDTO.setCode("**");
            roleDTO.setName("**");
            roleDTO.setStatus(Byte.parseByte("0"));
            roleDTOS[i] = roleDTO;
        }
        try {
            Boolean result2 = roleController.batchRemove(roleDTOS);
            Assert.assertTrue(result2);
        }catch (Exception e){
            e.printStackTrace();
        }
        // 测试步骤3
           // 测试参数数组为空的时候 是否触发异常
        try {
            roleController.batchRemove(null);
        }catch (Exception e){
            e.printStackTrace();
        }

        // 测试步骤4
           // 测试参数数组不为空 但是id为空的时候是否异常
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setStatus(Byte.parseByte("0"));
        roleDTO.setCode("**");
        roleDTO.setName("**");
        try {
            roleController.batchRemove(new RoleDTO[]{roleDTO});
        }catch (Exception e){
            e.printStackTrace();
        }
        // 测试步骤5
           // 传入数组中包含触发异常的测试数据 是否回滚事务，可以通过service的联合查询方法来验证
        RoleDTO[] roleDTOS2 = new RoleDTO[2];
        for (int i = 0; i < 2; i++) {
            Long tId = addRoleForTest();
            RoleDTO dto = new RoleDTO();
            dto.setId(tId);
            dto.setCode("**");
            dto.setName("**");
            dto.setStatus(Byte.parseByte("0"));
            roleDTOS2[i] = roleDTO;
        }
        roleDTOS2[1].setName(null);
        try{
            roleController.batchRemove(roleDTOS2);
            Role role = roleMapper.selectById(roleDTOS2[0].getId());
            Assert.assertNotNull(role);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * @Description 测试控制器 modify 方法
     * @author Gayrot
     * @date 2023/6/16 12:59
     */
    @Test
    void modify() {
        //生成测试角色
        Long testRoleId = addRoleForTest();
        // 测试步骤1
             // 测试传入必输项完整的RoleDTO对象，成功修改
        RoleDTO roleDto1 = new RoleDTO();
        roleDto1.setId(testRoleId);
        roleDto1.setVersion(0L);
        roleDto1.setName("alreadyModify");
        roleDto1.setCode("modify");
        roleDto1.setCreatedBy(000L);
        roleDto1.setCreatedTime(new Date());
        try{
            Boolean modify = roleController.modify(roleDto1);
            Assert.assertTrue(modify);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 测试步骤2
            // 测试传入必输项目不完整的对象，校验不通过触发异常
        RoleDTO roleDto2 = new RoleDTO();
        roleDto2.setId(testRoleId);
        roleDto2.setVersion(0L);
        roleDto2.setName("alreadyModify");
        roleDto2.setCreatedBy(000L);
        roleDto2.setCreatedTime(new Date());
        try{
            Boolean modify = roleController.modify(roleDto2);
            Assert.assertTrue(modify);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 测试步骤3
           // 测试传入编号重复的角色对象 修改不成功
        RoleDTO roleDto3 = new RoleDTO();
        roleDto3.setId(testRoleId);
        roleDto3.setVersion(0L);
        roleDto3.setName("CodeHasExist");
        roleDto3.setCode("000");
        roleDto3.setCreatedBy(000L);
        roleDto3.setCreatedTime(new Date());
        try{
            Boolean modify = roleController.modify(roleDto3);
            Assert.assertTrue(modify);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @Description 测试控制器 assignResource方法
     * @author Gayrot
     * @date 2023/6/16 13:50
     */
    @Test
    void assignResource() {
        //生成一个测试角色
        Long id1 = addRoleForTest();
        //测试步骤1
            // 测试为一个新的角色分配 5条系统资源 分配成功
        RoleResourceDTO dto = new RoleResourceDTO();
        dto.setRoleIds(Arrays.asList(id1));
        dto.setResourceIds(Arrays.asList(1211510578092395986L,
                1211510578092397546L,
                1211792719418951569L,
                1211792719418952895L,
                1211792719418954568L));
        try{
            roleController.assignResource(dto);
        }catch (Exception e) {
            e.printStackTrace();
        }
        int hasResourceNum1 = getResourceNumber(id1);
        Assert.assertEquals(5,hasResourceNum1);

        //测试步骤2
            //　测试为一个已经分配资源的角色新增３条资源　分配成功　资源数量正确
        RoleResourceDTO dto1 = new RoleResourceDTO();
        dto1.setRoleIds(Arrays.asList(id1));
        dto1.setResourceIds(Arrays.asList(1211510578092395986L,
                1211510578092397546L,
                1211792719418951569L,
                1211792719418952895L,
                1211792719418954568L,
                1211792719418957900L,
                1211792719418957901L,
                1215906631262712109L));
        try{
            roleController.assignResource(dto1);
        }catch (Exception e) {
            e.printStackTrace();
        }
        int hasResourceNum2 = getResourceNumber(id1);
        Assert.assertEquals(8,hasResourceNum2);

        //测试步骤3
           // 测试为已经存在资源的角色 去除1条原有资源 加入2条新的资源，分配成功，资源正确
        RoleResourceDTO dto2 = new RoleResourceDTO();
        dto2.setRoleIds(Arrays.asList(id1));
        dto2.setResourceIds(Arrays.asList(
                1211510578092397546L,
                1211792719418951569L,
                1211792719418952895L,
                1211792719418954568L,
                1211792719418957900L,
                1211792719418957901L,
                1215906631262712109L,
                1219427937925091833L,
                1233024044473053652L));
        try{
            roleController.assignResource(dto2);
        }catch (Exception e) {
            e.printStackTrace();
        }
        int hasResourceNum3 = getResourceNumber(id1);
        Assert.assertEquals(9,hasResourceNum3);

        //测试步骤4
           // 测试为２个角色同时分配 3条新资源，分配成功
        //生成两个测试角色
        Long id2 = addRoleForTest();
        Long id3 = addRoleForTest();
        RoleResourceDTO dto3 = new RoleResourceDTO();
        dto3.setRoleIds(Arrays.asList(id2,id3));
        dto3.setResourceIds(Arrays.asList(
                1215906631262712109L,
                1219427937925091833L,
                1233024044473053652L));
        try{
            roleController.assignResource(dto3);
        }catch (Exception e) {
            e.printStackTrace();
        }
        int hasResourceNum4 = getResourceNumber(id2);
        int hasResourceNum5 = getResourceNumber(id3);
        Assert.assertEquals(3,hasResourceNum4);
        Assert.assertEquals(3,hasResourceNum5);

        //测试步骤5
           //测试传入RoleResourceDTO 的角色列表为空 ，校验失败触发异常
        RoleResourceDTO dto4 = new RoleResourceDTO();
        dto4.setRoleIds(null);
        dto4.setResourceIds(Arrays.asList(
                1215906631262712109L,
                1219427937925091833L,
                1233024044473053652L));
        try{
            roleController.assignResource(dto4);
        }catch (Exception e) {
            e.printStackTrace();
        }

        //测试步骤６
          // 测试传入RoleResourceDTO 的资源列表为空 ，分配成功角色所对应资源被清除
        RoleResourceDTO dto5 = new RoleResourceDTO();
        dto5.setRoleIds(Arrays.asList(id2,id3));
        dto5.setResourceIds(Collections.emptyList());
        try{
            roleController.assignResource(dto5);
        }catch (Exception e) {
            e.printStackTrace();
        }
        int hasResourceNum6 = getResourceNumber(id2);
        int hasResourceNum7 = getResourceNumber(id3);
        Assert.assertEquals(0,hasResourceNum6);
        Assert.assertEquals(0,hasResourceNum7);

        // 测试步骤7 测试角色分配资源的事务性控制
          // 测试传入 RoleResourceDTO 其中包含一个可能触发异常的值，事务被回滚
        RoleResourceDTO dto6 = new RoleResourceDTO();
        //900角色不存在，触发异常，回滚
        dto6.setRoleIds(Arrays.asList(id1,900L));
        dto6.setResourceIds(Collections.emptyList());
        try{
            roleController.assignResource(dto3);
        }catch (Exception e) {
            e.printStackTrace();
        }
        int hasResourceNum8 = getResourceNumber(id1);
        Assert.assertEquals(9,hasResourceNum8);
    }

    /**
     * @Description   测试分配用户方法
     * @author Gayrot
     * @date 2023/6/16 16:39
     */
    @Test
    void assignUser() {
        //生成一个新角色
        Long testRoleId = addRoleForTest();
        //测试步骤1
        // 测试为一个新的角色分配 5条系统用户 分配成功
        UserRoleDTO dto = new UserRoleDTO();
        dto.setRoleIds(Arrays.asList(testRoleId));
        dto.setUserIds(Arrays.asList(
                1595668343841488896L,
                1596736050422743040L,
                1597075281032642560L,
                1597075334589710336L,
                1597075336162574336L));
        try {
            roleController.assignUser(dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        int userNum1 = getUserNumber(testRoleId);
        Assert.assertEquals(5,userNum1);

        //测试步骤2
        //　测试为一个已经分配资源的角色新增３条系统用户　分配成功　系统用户数量正确
        dto.setRoleIds(Arrays.asList(testRoleId));
        dto.setUserIds(Arrays.asList(
                1595668343841488896L,
                1596736050422743040L,
                1597075281032642560L,
                1597075334589710336L,
                1597075336162574336L,
                1597075339677401088L,
                1597075367179452416L,
                1597075590475808768L));
        try {
            roleController.assignUser(dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        int userNum2 = getUserNumber(testRoleId);
        Assert.assertEquals(8,userNum2);

        //测试步骤3
        // 测试为已经存在资源的角色  加入2条新的系统用户，分配成功，系统用户正确
        dto.setRoleIds(Arrays.asList(testRoleId));
        dto.setUserIds(Arrays.asList(
                1596736050422743040L,
                1597075281032642560L,
                1597075334589710336L,
                1597075336162574336L,
                1597075339677401088L,
                1597075367179452416L,
                1597075590475808768L,
                1597075741764354048L,
                1597075836543041536L));
        try {
            roleController.assignUser(dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        int userNum3 = getUserNumber(testRoleId);
        Assert.assertEquals(10,userNum3);

        //测试步骤4
        // 测试为２个角色同时分配 3条新系统用户，分配成功
        Long testRoleId2 = addRoleForTest();
        Long testRoleId3 = addRoleForTest();
        dto.setRoleIds(Arrays.asList(testRoleId2,testRoleId3));
        dto.setUserIds(Arrays.asList(1597075967682150400L,
                1597076106333257728L,
                1597076226734948352L));
        try {
            roleController.assignUser(dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        int userNum4 = getUserNumber(testRoleId2);
        int userNum5 = getUserNumber(testRoleId3);
        Assert.assertEquals(3,userNum4);
        Assert.assertEquals(3,userNum5);

        //测试步骤5
        //测试传入UserRoleDTO 的角色列表为空 ，校验失败触发异常
        dto.setRoleIds(null);
        try {
            roleController.assignUser(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //测试步骤６
        // 测试传入UserRoleDTO 的用户列表为空 ，分配成功角色所对应用户被清除
        dto.setRoleIds(Arrays.asList(testRoleId2));
        dto.setUserIds(Collections.emptyList());
        try {
            roleController.assignUser(dto);
        }catch (Exception e){
            e.printStackTrace();
        }
        int userNum6 = getUserNumber(testRoleId2);
        Assert.assertEquals(0,userNum6);

        // 测试步骤7 测试角色分配用户的事务性控制
        // 测试传入 RoleResourceDTO 其中包含一个可能触发异常的值，事务被回滚
        dto.setRoleIds(Arrays.asList(testRoleId, null));
        dto.setUserIds(Collections.emptyList());
        try {
            roleController.assignUser(dto);
        } catch (Exception e) {
            e.printStackTrace();
        }
        int userNum7 = getUserNumber(testRoleId);
        Assert.assertEquals(10,userNum7);
    }

    @Test
    void haveRole() {
        // 测试步骤1
           // 传入一个存在的角色的编号 返回存在
        //测试步骤2
           // 传入一个不存在的角色的编号 返回不存在
        // 测试步骤3
           // 传入的编号为空 触发异常
    }

    /**
     * @Description   针对控制器的commonQuery单元测试
     * @author Gayrot
     * @date 2023/6/16 16:40
     */
    @Test
    void commonQuery() {
        // 测试步骤1
            // 测试一个角色名模糊相等的 返回符合条件的记录 分页信息正确
        RoleQuery query = new RoleQuery();
        query.setName("管理员");
        query.setPageNum(1);
        query.setPageSize(10);
        try {
            CommonPage<RoleDTO> roleVOS = roleController.commonQuery(query);
            List<RoleDTO> list = roleVOS.getData();
            Assert.assertEquals(2,list.size());
        }catch (Exception e) {
            e.printStackTrace();
        }
        // 测试步骤2
           // 测试传入一个 空的查询条件，返回所有的记录
        RoleQuery query1 = new RoleQuery();
        query1.setPageNum(1);
        query1.setPageSize(10);
        try {
            CommonPage<RoleDTO> roleVOS = roleController.commonQuery(query1);
            List<RoleDTO> list = roleVOS.getData();
            Assert.assertEquals(10,list.size());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}