package com.sd365;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;
/**
 * @Class PermissionCentreApp
 * @Description
 * abel.zhan 2023-05-19 禁止 nacos命名logback
 * @Author Administrator
 * @Date 2023-05-19  10:09
 * @version 1.0.0
 */
@SpringBootApplication
@ComponentScans({
        @ComponentScan("com.sd365.common"),
        @ComponentScan("com.sd365.common.api.swagger")
})
@MapperScan(("com.sd365.permission.centre.dao.mapper"))
@EnableCaching
@EnableSwagger2
@EnableDiscoveryClient
@EnableRabbit
@EnableTransactionManagement
@RefreshScope
public class PermissionCentreApp {
    public static void main( String[] args )
    {
        // 禁用nacos的logback 否则将出现 logback context重名失败
        System.setProperty("nacos.logging.default.config.enabled","false");
        SpringApplication.run(PermissionCentreApp.class, args);
    }
}
