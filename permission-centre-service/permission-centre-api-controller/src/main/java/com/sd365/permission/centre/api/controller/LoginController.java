package com.sd365.permission.centre.api.controller;


import com.sd365.permission.centre.api.LoginApi;
import com.sd365.permission.centre.pojo.vo.LoginUserVO;
import com.sd365.permission.centre.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;


/**
 * @description: 退出登录修复
 * @author: wujiandong
 * @create: 2022/8/5 17:15
 * @version: v1.0.9
 **/
@RestController
@Slf4j
@Validated
public class LoginController  implements LoginApi {

    /**
     * 登录服务
     */
    @Autowired
    private LoginService loginService;

    @Override
    public LoginUserVO auth(@RequestParam(value = "code") String code, @RequestParam(value = "password") String password, @RequestParam(value = "account") String account) {

        return loginService.authHandler(code,password,account);
    }


    @Override
    public LoginUserVO login(@NotNull @RequestParam(value = "code") String code,
                        @NotNull @RequestParam(value = "password") String password,
                        @NotNull @RequestParam(value = "account") String account) {

        return loginService.loginHandler(code,password,account);

    }
    @Override
    public LoginUserVO info(@RequestParam(value = "code") String code,
                            @RequestParam(value = "account") String account) {

        return loginService.getUserInfoHandler(code,account);

    }
    @Override
    public LoginUserVO logout() {

        return loginService.logoutHandler();
    }
}
