package com.sd365.permission.centre.api.controller;

import com.sd365.permission.centre.api.CacheApi;
import com.sd365.permission.centre.pojo.vo.CacheInfoVO;
import com.sd365.permission.centre.pojo.vo.KeyValueVo;
import com.sd365.permission.centre.service.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@Slf4j
public class CacheController implements CacheApi {
    /**
     * 缓存服务类
     */
    @Autowired
    private CacheService cacheService;
    @Override
    public CacheInfoVO getCacheInfo(String key) {
        return cacheService.getCacheInfo(key);
    }
    @Override
    public KeyValueVo queryByKey(String key) {
        return cacheService.queryByKey(key);
    }
    @Override
    public Boolean deleteByKey(String key) {
        return cacheService.deleteByKey(key);
    }

}