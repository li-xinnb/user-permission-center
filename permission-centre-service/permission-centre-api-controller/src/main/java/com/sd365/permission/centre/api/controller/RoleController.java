package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.RoleApi;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.dto.*;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.*;
import com.sd365.permission.centre.service.RoleService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description 角色管理api 主要包含 角色的增删除改查以及角色和资源绑定，角色和用户绑定
 * @Class RoleController
 * @author Gayrot
 * @date 2024/4/22 11:23
 */

@CrossOrigin
@RestController
public class RoleController implements RoleApi {
    /**
     * 角色服务类
     */
    @Autowired
    private RoleService roleService;
    @Override
    public Boolean initRoleResourceRedis(@NotNull @ApiParam(name = "role",type = "Role",required = true) Role role) {
        return roleService.loadRoleResourceCache(role);
    }

    @ApiLog
    @Override
    public Boolean assignResource(@NotNull @Valid @RequestBody RoleResourceDTO roleResourceDTO) {
        return roleService.assignResource(roleResourceDTO);
    }
    @ApiLog
    @Override
    public Boolean assignUser(@NotNull @Valid @RequestBody UserRoleDTO userRoleDTO) {
        return roleService.assignUser(userRoleDTO);
    }

    @ApiLog
    @Override
    @Deprecated
    public List<Node> selectCompany() {
        return roleService.selectCompany();
    }

    @ApiLog
    @Override
    public Boolean add(@NotNull @Valid @RequestBody RoleDTO roleDTO) {
        return roleService.add(roleDTO);
    }

    @ApiLog
    @Override
    public Boolean remove(@ApiParam(value = "当前行id", required = true) @NotNull Long id,
                          @ApiParam(value = "当前行版本", required = true)@NotNull Long version) {
        return roleService.remove(id, version);
    }

    @ApiLog
    @Override
    public Boolean batchRemove(@NotNull @Valid @RequestBody RoleDTO[] roleDTOS) {
        return roleService.batchRemove(roleDTOS);
    }

    @ApiLog
    @Override
    public Boolean modify(@NotNull @Valid @RequestBody RoleDTO roleDTO) {
        return roleService.modify(roleDTO) != null;
    }

    @ApiLog
    @Override
    public RoleDTO getRoleDtoForCopy(@NotNull  Long id) {
        return roleService.getRoleDtoForCopy(id);
    }

    @ApiLog
    @Override
    public CommonPage<RoleDTO> commonQuery(@NotNull  RoleQuery roleQuery) {
        //执行查询
        return  roleService.commonQuery(roleQuery);
    }

    @ApiLog
    @Override
    public CommonPage<UserDTO> commonQueryUser(@NotNull UserQuery userQuery) {
        return  roleService.commonQueryUserForRoleBindingUsers(userQuery);
    }

    @ApiLog
    @Override
    public RoleVO queryRoleById(@NotNull Long id) {
        return roleService.queryById(id);
    }









    @ApiLog
    @Override
    public boolean haveRole(@NotNull RoleDTO roleDTO) {
        return roleService.haveRole(roleDTO);
    }

    @ApiLog
    @Override
    public List<Node> doQueryResourceByRoleId(@NotNull RoleQuery roleQuery) {
        return roleService.queryResourceByRoleId(roleQuery.getId());
    }

    @Override
    public RoleVO queryUserResource(@NotNull Long id) {
        RoleDTO roleDTO = roleService.queryUserResource(id);
        if (roleDTO != null) {
            RoleVO roleVO = BeanUtil.copy(roleDTO, RoleVO.class);
            roleVO.setResourceVOS( BeanUtil.copyList(roleDTO.getResourceDTOS(), ResourceVO.class));
            return roleVO;
        } else{ return new RoleVO();}
    }
}
