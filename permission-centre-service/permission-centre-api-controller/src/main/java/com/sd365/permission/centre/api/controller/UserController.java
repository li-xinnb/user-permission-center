/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName UserController.java
 * @Author Administrator
 * @Date 2022-9-28  21:35
 * @Description 该文件为具体的用户管理界面的对应的控制器类
 * History:
 * <author> Administrator
 * <time> 2022-9-28  21:35
 * <version> 1.0.0
 * <desc> 该文件为具体的用户管理界面的对应的控制器类
 */
package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.UserApi;
import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.entity.Position;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UpdatePasswordDTO;
import com.sd365.permission.centre.pojo.dto.UserCentreDTO;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import com.sd365.permission.centre.pojo.dto.UserSelfInfoDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.UserVO;
import com.sd365.permission.centre.service.LoginService;
import com.sd365.permission.centre.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
/**
 * @Description 用户管理接口
 * @author Gayrot
 * @date 2024/5/11 18:10
 */
@RestController
public class UserController implements UserApi {

    /**
     *  用户服务
     */
    @Autowired
    private UserService userSerice;
    /**
     *  登录服务
     */
    @Autowired
    LoginService loginService;

    @Override
    public List<ResourceVO> getUserResourceVO(Long id) {

        return loginService.getUserResourceTreeHandler(id);
    }

    @Override
    public List<ResourceVO> getRoleResourceVO(Long[] roleIdArray) {
        return loginService.getResourceVO(roleIdArray);
    }

    @ApiLog
    @Override
    public Boolean add(@RequestBody  @Valid UserDTO userDTO) {
        return userSerice.add(userDTO);
    }

    @ApiLog
    @Override
    public Boolean remove(Long id, Long version) {
        return userSerice.remove(id, version);
    }

    @ApiLog
    @Override
    public Boolean modify(@Valid UserDTO userDTO) {
        return userSerice.modify(userDTO);
    }

    @Override
    public Boolean modifyUserInfo(UserCentreDTO userCentreDTO) {
        return userSerice.modifyUserInfo(userCentreDTO);
    }

    @Override
    public Boolean modifyUserInfoForBm(UserCentreDTO userCentreDTO) {
        return userSerice.modifyUserInfoForBm(userCentreDTO);
    }

    @Override
    public Boolean modifyWithNewRole(UserDTO[] userDTOS) {
        return userSerice.modifyWithNewRole(userDTOS);
    }

    @ApiLog
    @Override
    public List<User> commonQuery(UserQuery userQuery) {
        return userSerice.commonQuery(userQuery);
    }


    @Override
    public UserVO queryUserById(Long id) {
        User user = userSerice.queryById(id);
       return user!=null ?  BeanUtil.copy(user, UserVO.class): new UserVO();

    }

    @Override
    public List<Role> queryAllRole() {
        return userSerice.queryAllRole();
    }

    @Override
    public List<Department> queryAllDepartment(DepartmentQuery departmentQuery) {
        return userSerice.queryAllDepartment(departmentQuery);
    }

    @Override
    public List<Position> queryAllPosition(PositionQuery positionQuery) {
        return userSerice.queryAllPosition(positionQuery);
    }

    @Override
    public Boolean batchUpdate(@Valid UserDTO[] userDTOS) {
        return userSerice.batchUpdate(userDTOS);
    }

    @Override
    public Boolean batchDelete(@Valid UserDTO[] userDTOS) {
        return userSerice.batchDelete(userDTOS);
    }

    @Override
    public Boolean firstStartMd5() {
        return true;
    }

    @Override
    public Boolean updatePassword(@Valid UserDTO userDTO) {
        return userSerice.updatePassword(userDTO);
    }

    @Override
    public User getUser(String code) {
        return userSerice.getUser(code);
    }

    @ApiLog
    @Override
    public Boolean updateMyselfInfo(@Valid UserSelfInfoDTO userDTO) {
        return userSerice.updateMyselfInfo(userDTO);
    }

    @Override
    public Boolean updatePassword(UpdatePasswordDTO userDTO) {
        return userSerice.updatePassword(userDTO);
    }
}
