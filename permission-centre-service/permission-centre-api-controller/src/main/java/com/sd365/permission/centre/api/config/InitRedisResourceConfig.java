package com.sd365.permission.centre.api.config;

import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.ResourceService;
import com.sd365.permission.centre.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author Gayrot
 * @Description 项目启动时自动把资源加载到缓存
 * @date 2023/06/16 18:27
 **/
@Slf4j
@Component
public class InitRedisResourceConfig {

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private RoleService roleService;

    @PostConstruct
    public void initRedisResource(){
        log.info("开始进行缓存资源加载.........");
        //加载角色
        roleService.loadRoleResourceCache(new Role());
        log.info("缓存资源加载========>角色-资源加载成功.........");

        //加载资源
        resourceService.loadResourceCache(new ResourceQuery());
        log.info("缓存资源加载========>资源加载成功.........");

        //加载公共资源，即所有人都有权访问的资源
        resourceService.loadCommonResources();
        log.info("缓存资源加载=========>公共资源加载成功.......");
    }
}
