package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.UserOnlineApi;
import com.sd365.permission.centre.pojo.dto.UserOnlineDTO;
import com.sd365.permission.centre.pojo.query.UserOnlineQuery;
import com.sd365.permission.centre.pojo.vo.UserOnlineVO;
import com.sd365.permission.centre.service.UserOnlineService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Author: WengYu
 * @CreateTime: 2022/06/22 17:47
 */
@RestController
@Slf4j
public class UserOnlineController implements UserOnlineApi {

    @Autowired
    private UserOnlineService userOnlineService;

    @ApiLog
    @Override
    @Pagination
    public CommonPage<UserOnlineVO> commonQuery(UserOnlineQuery userOnlineQuery) {
        return userOnlineService.getOnlineUsers(userOnlineQuery);
    }

    @Override
    public void forceLogout(@RequestParam("ids") String ids) {
        if (!StringUtils.isEmpty(ids)){
            userOnlineService.forceLogout(ids);
        }
    }

}
