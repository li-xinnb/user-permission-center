package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.DepartmentApi;
import com.sd365.permission.centre.pojo.dto.DepartmentDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.vo.DepartmentVO;
import com.sd365.permission.centre.service.DepartmentService;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author jxd
 * @Date 2020/12/12  1:05 下午
 * @Version 1.0
 * @Write For DepartmentController
 * @Email waniiu@126.com
 */
@Slf4j
@RestController
public class DepartmentController implements DepartmentApi {

    @Resource
    DepartmentService departmentService;

    @ApiLog
    @Override
    public List<DepartmentVO> commonQuery(@ApiParam @NotNull @RequestBody DepartmentQuery departmentQuery) {
        if (null == departmentQuery) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<DepartmentDTO> departmentDTOS = departmentService.commonQuery(departmentQuery);
        List<DepartmentVO> departmentVOS = BeanUtil.copyList(departmentDTOS, DepartmentVO.class);
        return departmentVOS;
    }


    @ApiLog
    @Override
    public Boolean add(@NotNull @Valid @RequestBody DepartmentDTO departmentDTO) {

        log.info("name:" + departmentDTO.getCreator());
        return departmentService.add(departmentDTO);
    }

    @ApiLog
    @Override
    public Boolean update(@NotNull @Valid @RequestBody DepartmentDTO departmentDTO) {
        return departmentService.modify(departmentDTO);
    }

    @ApiLog
    @Override
    public Boolean deleteOneById(Long id, Long version) {
        return departmentService.remove(id, version);
    }

    @ApiLog
    @Override
    public DepartmentVO queryDepartmentById( @NotNull @PathVariable("id") Long id) {
        //throw new BusinessException(CommonErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("测试异常"));
        DepartmentDTO departmentDTO = departmentService.queryById(id);
        if (departmentDTO != null) {
            DepartmentVO departmentVO = BeanUtil.copy(departmentDTO, DepartmentVO.class);
            return departmentVO;
        } else {
            return null;
        }
    }

    @Override
    public Boolean deleteBatch(@NotEmpty @RequestBody List<IdVersionQuery> batchQuery) {
        return departmentService.deleteBatch(batchQuery);
    }

    @Override
    public DepartmentDTO copy(@NotNull Long id) {
        return departmentService.copy(id);
    }


    @Override
    @ApiLog
    public CommonPage<DepartmentVO> commonQueryDepartment(@NotNull DepartmentQuery departmentQuery) {

        return departmentService.commonQueryDepartment(departmentQuery);
    }

    @Override
    @ApiLog
    public Boolean batchUpdate(@NotEmpty @RequestBody List<DepartmentDTO> departmentDTOS) {

        Boolean flag = true;
        // 批量删除
        for (int i = 0; i < departmentDTOS.size(); ++i) {
            DepartmentDTO dto = departmentDTOS.get(i);
            flag = departmentService.updateStatus(dto);
            if (!flag) {
                return false;
            }
        }
        return flag;
    }

    /**
     * 更新单条启用状态
     *
     * @param departmentDTO 部门DTO
     * @return
     * @author Yan Huazhi
     * @date 2020/12/18 9:20
     * @version 0.0.1
     */
    @Override
    @ApiLog
    public Boolean status(@NotNull @RequestBody DepartmentDTO departmentDTO) {
        return departmentService.updateStatus(departmentDTO);
    }
    @ApiLog
    @Override
    public List<DepartmentDTO> builderTree(@NotNull DepartmentQuery departmentQuery) {
        return departmentService.builderTree(departmentQuery);
    }
}
