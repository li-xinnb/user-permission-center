package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.PositionApi;
import com.sd365.permission.centre.pojo.dto.DeletePositionDTO;
import com.sd365.permission.centre.pojo.dto.PositionDTO;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import com.sd365.permission.centre.pojo.vo.PositionVO;
import com.sd365.permission.centre.service.PositionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * @ClassName PositionController
 * @Description 
 * @Author Gayrot
 * @Date 2024/04/01 11:11
 * @Version 1.0
 **/
@CrossOrigin
@RestController
public class PositionController extends AbstractController implements PositionApi {

    @Autowired
    private PositionService positionService;


    @Override
    public Boolean add(@Valid PositionDTO positionDTO) {
        if (null == positionDTO) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("添加对象不允许为空"));
        }
        return positionService.add(positionDTO);
    }

    @Override
    public Boolean remove(Long id, Long version) {
        return positionService.remove(id, version);
    }

    @Override
    public Boolean modify(@Valid PositionDTO positionDTO) {
        if (null == positionDTO) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("修改对象不允许为空"));
        }
        return positionService.modify(positionDTO);
    }
    @ApiLog
    @Override
    public List<PositionVO> commonQuery(PositionQuery positionQuery) {
        if (null == positionQuery) {
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID, new Exception("查询对象不允许为空"));
        }
        List<PositionDTO> positionDTOS= positionService.commonQuery(positionQuery);
        List<PositionVO> positionVOS= BeanUtil.copyList(positionDTOS, PositionVO.class);
        return positionVOS;
    }

    @Override
    public PositionVO queryPositionById(Long id) {
        PositionDTO positionDTO = positionService.queryById(id);
        if (positionDTO!=null){
            PositionVO positionVO = BeanUtil.copy(positionDTO,PositionVO.class);
            return positionVO;
        }else {
            return null;
        }
    }

    @Override
    public Boolean batchUpdate(@Valid PositionDTO[] positionDTOS) {
        return positionService.batchUpdate(positionDTOS);
    }

    @Override
    public Boolean batchDelete(@Valid DeletePositionDTO[] deletePositionDTOS) {
        return positionService.batchDelete(deletePositionDTOS);
    }
}
