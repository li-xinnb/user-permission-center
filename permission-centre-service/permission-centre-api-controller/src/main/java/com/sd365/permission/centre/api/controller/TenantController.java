package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.TenantApi;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import com.sd365.permission.centre.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class TenantController
 * @Description 租户实现类，接口的功能请参考接口注释
 * @Author Administrator
 * @Date 2023-03-15  8:24
 * @version 1.0.0
 */
@RestController
public class TenantController implements TenantApi {
    /**
     *  租户服务
     */
    @Autowired
    private TenantService tenantService;

    @Override
    public Boolean add(TenantDTO tenantDTO) {
        return tenantService.add(tenantDTO);
    }

    @ApiLog
    @Override
    public List<TenantVO> commonQuery(TenantQuery tenantQuery) {
        return BeanUtil.copyList(tenantService.commonQuery(tenantQuery), TenantVO.class);

    }

    @Override
    public Boolean remove(Long id, Long version) {
        return tenantService.remove(id,version);
    }

    @Override
    public Boolean modify(TenantDTO tenantDTO) {
        return tenantService.modify(tenantDTO);
    }

    @Override
    public Boolean updateStatus(TenantDTO tenantDTO) {
        return tenantService.updateStatus(tenantDTO);
    }

    @Override
    public Boolean batchRemove(DeleteTenantDTO[] deleteTenantDTOS) {
        return tenantService.batchDelete(deleteTenantDTOS);
    }

    @Override
    public TenantVO queryTenantById(Long id) {
        //throw new BusinessException(CommonErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("测试异常"));
        TenantDTO tenantDTO = tenantService.queryById(id);
        return tenantDTO!=null  ? BeanUtil.copy(tenantDTO,TenantVO.class) :new TenantVO() ;

    }

    @Override
    public TenantVO queryByName(String name) {
        TenantDTO tenantDTO=tenantService.queryByName(name);
        return tenantDTO!=null ? BeanUtil.copy(tenantDTO,TenantVO.class) : new TenantVO();
    }
}
