package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.AdministrativeApi;
import com.sd365.permission.centre.entity.Administrative;
import com.sd365.permission.centre.pojo.dto.AdministrativeDTO;
import com.sd365.permission.centre.pojo.query.AdministrativeQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.vo.AdministrativeVO;
import com.sd365.permission.centre.service.AdministrativeService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class AdministrativeController
 * @Description 区划对象后台管理接口类，主要包含了增删改查以及批量删除功能。
 * @Author Administrator
 * @Date 2023-02-14  11:11
 * @version 1.0.0
 */
@RestController
@Validated
public class AdministrativeController implements AdministrativeApi {
    /**
     * 区划服务
     */
    @Resource
    private  AdministrativeService administrativeService;
    @Override
    public Boolean add( @NotNull @Valid @RequestBody  AdministrativeDTO administrativeDTO) {
        return administrativeService.add(administrativeDTO);
    }
    @Override
    public Boolean remove(@NotNull Long id, @NotNull Long version) {
            return administrativeService.remove(id, version);
    }

    @Override
    public Boolean modify(@NotNull @Valid @RequestBody AdministrativeDTO administrativeDTO) {
        return administrativeService.modify(administrativeDTO);
    }

    /**
     * 通用查询，带分页信息
     *
     * @param administrativeQuery
     * @return
     */
    @Override
    @ApiLog
    public List<AdministrativeVO> commonQuery(@NotNull  AdministrativeQuery administrativeQuery) {
        List<Administrative> administratives = administrativeService.commonQuery(administrativeQuery);
        return  BeanUtil.copyList(administratives, AdministrativeVO.class);
    }

    @Override
    @ApiLog
    public AdministrativeVO selectById(@NotNull @PathVariable("id") Long id) {
      return  BeanUtil.copy(administrativeService.queryById(id), AdministrativeVO.class);
    }

    @Override
    @ApiLog
    public Boolean batchDelete(@NotEmpty @RequestBody List<IdVersionQuery> idVersionQueryList) {
        return administrativeService.batchDelete(idVersionQueryList);
    }

    @Override
    public Boolean status(@NotNull @Valid @RequestBody AdministrativeDTO administrativeDTO) {
        return administrativeService.modify(administrativeDTO);
    }
}
