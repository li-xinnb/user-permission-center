/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName UserApi.java
 * @Author Administrator
 * @Date 2022-9-28  20:56
 * @Description 文件定义了用户管理界面的接口，主要包括用户增改查以及用户角色分配等
 * History:
 * <author> Administrator
 * <time> 2022-9-28  20:56
 * <version> 1.0.0
 * <desc> 文件定义了用户管理界面的接口，主要包括用户增改查以及用户角色分配等
 */
package com.sd365.permission.centre.api;
import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.entity.Position;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UpdatePasswordDTO;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import com.sd365.permission.centre.pojo.dto.UserCentreDTO;
import com.sd365.permission.centre.pojo.dto.UserSelfInfoDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.pojo.vo.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Description 用户管理接口 主要是包含增删改查以及对用户分配角色
 * @author Gayrot
 * @date 2024/3/11 16:28
 */
@CrossOrigin
@Api(tags = "用户管理 ", value = "/permission/centre/v1/user")
@RequestMapping(value = "/permission/centre/v1/user")
public interface UserApi {
    /**
     * 获取用户的资源树
     * @param: 用户id
     * @return: 用户所对应的角色的权限列表
     */
    @GetMapping(value = "/getUserResource")
    List<ResourceVO> getUserResourceVO(Long id);

    /**
     * 网关的鉴权服务需要依据角色从用户中心取得资源
     * @param roleIdArray
     * @return 资源列表
     */
    @GetMapping(value = "/getRoleResource")
    List<ResourceVO> getRoleResourceVO(Long[] roleIdArray);
    /**
     * 用户管理界面增加页面保存数据调用此接口
     * @param userDTO 用户对象
     * @return true成功 false失败 通一个租户下工号不可以重复
     */
    @ApiOperation(tags = "增加用户", value = "/add")
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add( @Valid @RequestBody UserDTO userDTO);
    /**
     * 删除用户
     * @param: 删除用户，若有对应角色则中间表用户-角色也需一并删除
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "删除用户", value = "/remove")
    @DeleteMapping(value = "/remove")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true)  @RequestParam("id")  Long id,
                   @ApiParam(value = "当前行版本", required = true)@NotNull @RequestParam("version") Long version);

    /**
     * 修改用户
     * @param: 用户DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "修改用户", value = "/update")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean modify(@Valid @RequestBody UserDTO userDTO);

    /**
     *  前端修改用户信息
     * @param: UserCentreDTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "修改用户", value = "/userInfo")
    @PutMapping(value = "/userInfo")
    @ResponseBody
    Boolean modifyUserInfo(@Valid @RequestBody UserCentreDTO userCentreDTO);

    /**
     *  Study BusinessManage platform 调用此接口修改用户信息，增加此是为了避免影响其原接口
     * @param userCentreDTO  其中不传送密码
     * @return true成功  false失败
     */
    @ApiOperation(tags = "修改用户", value = "/userInfoBm")
    @PutMapping(value = "/userInfoBm")
    @ResponseBody
    Boolean modifyUserInfoForBm(@Valid @RequestBody UserCentreDTO userCentreDTO);
    /**
     *  修改用户角色
     * @param: 用户DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "修改用户角色", value = "/modifyWithNewRole")
    @PutMapping(value = "/modifyWithNewRole")
    @ResponseBody
    Boolean modifyWithNewRole(@NotNull @RequestBody UserDTO[] userDTOS);
    /**
     * 查询用户信息
     * @param userQuery 查询条件
     * @return 用户列表
     */
    @ApiOperation(tags = "查询用户", value = "/commonQuery")
    @GetMapping(value = "/commonQuery")
    @ResponseBody
    List<User> commonQuery(UserQuery userQuery);

    /**
     * 通过id取得用户
     * @param id  用户id
     * @return 用户对象
     */
    @ApiOperation(tags = "查询用户通过ID", value = "/{id}")
    @GetMapping(value = "/{id}")
    @ResponseBody
    UserVO queryUserById(@PathVariable("id") Long id);

    /**
     *  获取所有的角色的信息 ，用户模块实现此接口有待商榷
     * @return 所有的角色信息
     */
    @ApiOperation(tags = "查询所有角色", value = "")
    @GetMapping(value = "/queryAllRole")
    @ResponseBody
    List<Role> queryAllRole();

    /**
     * 查询所有的部门，TODO 需要增加租户的作为条件
     * @return 所有的部门信息 用户模块实现此接口有待商榷
     */
    @ApiOperation(tags = "查询所有部门", value = "")
    @GetMapping(value = "/queryAllDepartment")
    @ResponseBody
    List<Department> queryAllDepartment(@NotNull DepartmentQuery departmentQuery);

    /**
     *  查询所有的职位信息 TODO 需要增加租户的作为条件
     * @return 职位信息
     */
    @ApiOperation(tags = "查询所有职位信息", value = "")
    @GetMapping(value = "/queryAllPosition")
    @ResponseBody
    List<Position> queryAllPosition(@NotNull PositionQuery positionQuery);

    /**
     * @param: 用户DTO数组
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "批量修改用户", value = "")
    @PutMapping(value = "/batchUpdate")
    @ResponseBody
    Boolean batchUpdate(@NotNull @RequestBody UserDTO[] userDTOS);

    /**
     * @param: 用户DTO数组
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "批量删除用户", value = "")
    @DeleteMapping(value = "/batchDelete")
    @ResponseBody
    Boolean batchDelete(@NotNull @RequestBody UserDTO[] userDTOS);

    /**
     * @Description: TODO
     * @Author: Administrator
     * @DATE: 2022-9-28  20:58
     * @param: 
     * @return: 
     */
    @ApiOperation(tags = "启用md5", value = "")
    @GetMapping(value = "/firstStartMd5")
    @ResponseBody
    Boolean firstStartMd5();
    /**
     * @Description: 重新更新密码，本次培训项目不需要实现这个
     * @Author: Administrator
     * @DATE: 2022-9-28  20:58
     * @param: 主要包含用户的id和新旧的密码
     * @return: 成功true 失败false
     */
    @ApiOperation(tags = "updatePassword", value = "")
    @PutMapping(value = "/upwd")
    Boolean updatePassword(@RequestBody @Valid UserDTO userDTO);

    /**
     * 找回用户密码接口
     * @param code  工号
     * @return 用户信息
     */
    @ApiOperation(tags = "找回密码", value = "")
    @GetMapping(value = "/getUserByCode")
    User getUser(@RequestParam(value = "code") String code);

    @ApiOperation(tags = "修改个人信息", value = "/updateMyselfInfo")
    @PutMapping(value = "/updateMyselfInfo")
    @ResponseBody
    Boolean updateMyselfInfo(@Valid @RequestBody UserSelfInfoDTO userDTO);

    @ApiOperation(tags = "修改密码", value = "/updatePassword")
    @PutMapping(value = "/updatePassword")
    @ResponseBody
    Boolean updatePassword(@Valid @RequestBody UpdatePasswordDTO userDTO);
}
