package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description 租户数据管理接口
 * @author Gayrot
 * @date 2024/4/1 10:34
 */
@CrossOrigin
@Validated
@Api(tags = "租户管理",value = "/permission/centre/v1/tenant")
@RequestMapping(value = "/permission/centre/v1/tenant")
public interface TenantApi {

    /**
     * @Description 增加租户
     * @author Gayrot
     * @date 2024/4/1 10:35
     * @param tenantDTO
     * @return Boolean
     */
    @ApiOperation(tags="增加租户",value="/add")
    @PostMapping(value="/add")
    @ResponseBody
    Boolean add(@NotNull  @Valid  @RequestBody TenantDTO tenantDTO);

    /**
     * 查询租户 支持多条件查询
     * @param tenantQuery
     * @return 租户列表
     */
    @ApiOperation(tags="查询租户",value="/commonQuery")
    @GetMapping(value = "/commonQuery")
    @ResponseBody
    List<TenantVO> commonQuery(@NotNull TenantQuery tenantQuery);

    /**
     *  删除租户
     * @param id  当前行的记录
     * @param version 当前行的版本
     * @return true成功 false失败
     */
    @ApiOperation(tags="删除租户",value="/remove")
    @DeleteMapping(value="/remove")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required=true) @NotNull @RequestParam("id") Long id,
                   @ApiParam(value="当前行版本",required = true)@NotNull @RequestParam("version") Long version);

    /**
     * 门户或者后台修改租户
     * @param tenantDTO
     * @return
     */
    @ApiOperation(tags="修改租户",value="/update")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean modify( @NotNull @Valid @RequestBody TenantDTO tenantDTO);

    /**
     * @Description 更改租户状态-启停
     * @author Gayrot
     * @date 2024/4/1 11:23
     * @param tenantDTO
     * @return Boolean
     */
    @ApiOperation(tags="修改租户-启停",value="/status")
    @PutMapping(value = "/status")
    @ResponseBody
    Boolean updateStatus(@NotNull @Valid @RequestBody TenantDTO tenantDTO);

    /**
     * 批量删除租户
     * @param deleteTenantDTOS  租户列表 因为要带版本和id所以使用对象数组
     * @return true成功false失败
     */
    @ApiOperation(tags="批量删除租户",value="/batch")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchRemove(@NotNull @Valid @RequestBody DeleteTenantDTO[] deleteTenantDTOS);

    /**
     * 通过id 查询租户
     * @param id 租户记录id
     * @return 租户对象
     */


    @ApiOperation(tags="查询租户通过ID",value="")
    @GetMapping(value = "/{id}")
    @ResponseBody
    TenantVO queryTenantById(@NotNull @PathVariable("id") Long id);

    /**
     *  查询是否存在同名租户用于判断租户
     * @param name  租户名字
     * @return 租户数据
     */
    @ApiOperation(tags = "查询是否存在租户名", value="")
    @GetMapping(value = "/queryByName")
    @ResponseBody
    TenantVO queryByName(String name);
}
