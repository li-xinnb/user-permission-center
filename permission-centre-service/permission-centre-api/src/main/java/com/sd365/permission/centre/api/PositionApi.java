package com.sd365.permission.centre.api;


import com.sd365.permission.centre.pojo.dto.DeletePositionDTO;
import com.sd365.permission.centre.pojo.dto.PositionDTO;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import com.sd365.permission.centre.pojo.vo.PositionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author Gayrot
 * @version 1.0.0
 * @class UserApi
 * @classdesc
 * @date 2024.03.21  20点28分
 */
@CrossOrigin
@Api(tags = "职位管理 ", value = "/permission/centre/v1/position")
@RequestMapping(value = "/permission/centre/v1/position")
public interface PositionApi {

    /**
     * @Description 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @author Gayrot
     * @date 2024/4/1 10:23
     * @param positionDTO
     * @return Boolean
     */
    @ApiOperation(tags = "增加职位", value = "/add")
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add(@RequestBody @Valid PositionDTO positionDTO);


    /**
     * @Description 删除职位
     * @author Gayrot
     * @date 2024/4/1 10:23
     * @param id
     * @param version
     * @return Boolean
     */
    @ApiOperation(tags = "删除职位", value = "/remove")
    @DeleteMapping(value = "/remove")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam("version") Long version);


    /**
     * @Description 修改职位 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @author Gayrot
     * @date 2024/4/1 10:24
     * @param positionDTO
     * @return Boolean
     */
    @ApiOperation(tags = "修改职位", value = "/update")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean modify(@Valid @RequestBody PositionDTO positionDTO);

    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:24
     * @param positionQuery
     * @return List<PositionVO>
     */
    @ApiOperation(tags = "查询职位", value = "/commonQuery")
    @GetMapping(value = "/commonQuery")
    @ResponseBody
    List<PositionVO> commonQuery(PositionQuery positionQuery);


    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:24
     * @param id
     * @return PositionVO
     */
    @ApiOperation(tags="查询职位通过ID",value="")
    @GetMapping(value = "/{id}")
    @ResponseBody
    PositionVO queryPositionById(@PathVariable("id") Long id);




    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:25
     * @param positionDTOS
     * @return Boolean
     */
    @ApiOperation(tags="批量修改职位",value="")
    @PutMapping(value = "/batchUpdate")
    @ResponseBody
    Boolean batchUpdate(@Valid @RequestBody PositionDTO[] positionDTOS);


    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:25
     * @param deletePositionDTOS
     * @return Boolean
     */
    @ApiOperation(tags="批量删除职位",value="")
    @DeleteMapping (value = "/batchDelete")
    @ResponseBody
    Boolean batchDelete(@Valid @RequestBody DeletePositionDTO[] deletePositionDTOS);

}
