package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.pojo.dto.MessageDTO;
import com.sd365.permission.centre.pojo.query.MessageQuery;
import com.sd365.permission.centre.pojo.vo.MessageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "消息管理", value = "/permission/centre/v1/message")
@RequestMapping(value = "/permission/centre/v1/message")
public interface MessageApi {

    /**
     * 消息板块
     *
     * @param messageQuery
     * @return
     */
    @ApiOperation(tags = "查询消息相关信息，分页", value = "")
    @GetMapping("")
    @ResponseBody
    List<MessageVO> commonQuery(MessageQuery messageQuery);

    /**
     * @param messageDTO
     * @return 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "增加消息", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@RequestBody @Valid MessageDTO messageDTO);

    /**
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags = "删除消息", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam("version") Long version);

    /**
     * @param messageDTO
     * @return
     */
    @ApiOperation(tags = "修改消息", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@Valid MessageDTO messageDTO);

    @ApiOperation(tags = "批量删除消息", value = "/batch")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    Boolean batchRemove(@Valid @RequestBody MessageDTO[] messageDTOS);

    @ApiOperation(tags = "批量更新消息", value = "/batch")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdate(@Valid @RequestBody MessageDTO[] messageDTOS);
}
