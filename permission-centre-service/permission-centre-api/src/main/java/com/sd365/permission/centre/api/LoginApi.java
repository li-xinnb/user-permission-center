/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: LoginApi
 * Author: Administrator
 * Date: 2022-10-8 18:07:44
 * Description:
 * <p> 重构了LoginController接口 新增加了接口给LoginController实现重构了RequestMapping URL
 * History:
 * <author> Administrator
 * <time> 2022-10-8 18:07:44
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.api;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.pojo.vo.LoginUserVO;
import org.springframework.web.bind.annotation.*;
import javax.validation.constraints.NotNull;


/**
 * @Description 提供登录服务，主要的调用者来自网关
 * @author Gayrot
 * @date 2023/6/16 16:51
 */
@RequestMapping(value = "/permission/centre/v1")
public interface LoginApi {

    /**
     * @Description 前端发起的 permission/centre/v1/user/login 但是在请求在 网关配置被转发到 认证
     * - id: gateway-authentication-service
     *   uri: lb://gateway-authentication
     *   predicates:
     * - Path=/permission/centre/v1/user/login
     *   filters:
     * - RewritePath=/permission/centre/v1/user/login, /auth/token
     * @author Gayrot
     * @date 2023/6/16 19:09
     * @param code 认证服务传递过来的 账号，
     * @param password 认证服务传递过来的 密码，
     * @param account 认证服务传递过来的 租户号
     * @return LoginUserVO 认证成功的用户信息内部包含数据 ，认证失败则返回对象但是对象内部无数据，前端需要依据UserVO内部数据判断
     */
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/auth")
    @ResponseBody
    LoginUserVO auth(@RequestParam(value = "code") String code, @RequestParam(value = "password") String password, @RequestParam(value = "account") String account);


    /**
     * @Description  前端请求登录服务
     * @author Gayrot
     * @date 2023/6/16 19:44
     * @param code
     * @param password
     * @param account
     * @return LoginUserVO
     */
    @Deprecated
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/login")
    @ResponseBody
    LoginUserVO login(@NotNull @RequestParam(value = "code") String code,
                        @NotNull @RequestParam(value = "password") String password,
                        @NotNull @RequestParam(value = "account") String account);

    /**
     * @Description 前端认证完成后调用此方法获取用户信息，用户信息包含权限信息
     * @author Gayrot
     * @date 2023/6/16 19:44
     * @param code 工号
     * @param account 租户账号
     * @return LoginUserVO
     */
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/info")
    @ResponseBody
    LoginUserVO info(@RequestParam(value = "code") String code, @RequestParam(value = "account") String account);


    /**
     * @Description  登出
     * @author Gayrot
     * @date 2023/6/16 19:56
     * @return LoginUserVO
     */
    @ApiLog
    @CrossOrigin
    @GetMapping(value = "/user/logout")
    @ResponseBody
    LoginUserVO logout();
}
