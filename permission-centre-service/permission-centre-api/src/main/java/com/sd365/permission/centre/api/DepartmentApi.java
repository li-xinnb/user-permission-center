package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.dto.DepartmentDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.vo.CompanyVO;
import com.sd365.permission.centre.pojo.vo.DepartmentVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Class DepartmentApi
 * @Description 部门管理接口，部门归属于公司，每个用户又都有一个部门
 * @Author Administrator
 * @Date 2023-02-21  16:11
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "部门 管理 ",value ="/permission/centre/v1/department")
@RequestMapping(value = "/permission/centre/v1/department")
@Validated
public interface DepartmentApi {

    /**
     * 通用查询
     * @param departmentQuery  查询条件一般对应查询区
     * @return 返回部门对象列表
     */
    @ApiOperation(tags = "查询部门信息，分页", value = "/commonQuery")
    @GetMapping("/commonQuery")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    CommonPage<DepartmentVO> commonQueryDepartment(@NotNull  DepartmentQuery departmentQuery);

    /**
     * 批量更新部门数据
     * @param departmentDTOS
     * @return true成功 false失败
     */
    @ApiOperation(tags="批量更新",value="/batch")
    @PutMapping(value = "/batch")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    Boolean batchUpdate(@NotEmpty @RequestBody List<DepartmentDTO> departmentDTOS);

    /**
     *  根据条件查询部门信息
     * @param departmentQuery 查询区参数
     * @return 部门列表
     */
    @ApiOperation(tags = "部门查询", value = "/departmentQuery")
    @GetMapping(value = "/departmentQuery")
    @ResponseBody
    @Deprecated
    List<DepartmentVO> commonQuery(@ApiParam @NotNull @RequestBody DepartmentQuery departmentQuery);

    /**
     *  新增部门
     * @param departmentDTO 部门对象
     * @return true成功 false失败
     */
    @ApiOperation(tags = "部门添加", value = "/add")
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add(@ApiParam @NotNull @Valid @RequestBody  DepartmentDTO departmentDTO);

    /**
     *  修改部门数据
     * @param departmentDTO 部门数据 修改依据是 id
     * @return true代表成功 false失败
     */
    @ApiOperation(tags = "部门修改", value = "/update")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean update(@NotNull @Valid @RequestBody DepartmentDTO departmentDTO);

    /**
     * 根据id 返回部门对象
     * @param id  记录id
     * @return 部门对象
     */
    @ApiOperation(tags = "查询department通过ID", value = "/{id}")
    @GetMapping(value = "/{id}")
    @ResponseBody
    DepartmentVO queryDepartmentById(@NotNull @PathVariable("id") Long id);

    /**
     * 根据id和版本删除记录
     * @param id  记录id
     * @param version  记录版本修改都增加1
     * @return
     */
    @ApiOperation(tags = "根据name删除单条数据", value = "/remove")
    @DeleteMapping(value = "/remove")
    @ResponseBody
    Boolean deleteOneById(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                          @ApiParam(value = "当前发行版本", required = true) @RequestParam("version") Long version);

    /**
     *  批量依据版本和id删除记录，如果同时删除只有一个人成功
     * @param batchQuery 查询条件
     * @return true成功 false失败
     */
    @ApiOperation(tags = "根据name删除多条数据", value = "/deleteBatch")
    @DeleteMapping(value = "/deleteBatch")
    @ResponseBody
    Boolean deleteBatch(@NotEmpty @RequestBody List<IdVersionQuery> batchQuery);


    @ApiOperation(tags = "拷贝部门", value = "/copy")
    @PostMapping(value = "/copy")
    @ResponseBody
    DepartmentDTO copy(@NotNull  Long id);

    @ApiOperation(tags = "通过id查询", value = "/status")
    @PutMapping(value = "/status")
    @ResponseBody
    public Boolean status(@NotNull @RequestBody DepartmentDTO departmentDTO);


    @ApiOperation(tags = "构建部门资源树", value = "/deptree")
    @GetMapping(value = "/deptree")
    @ResponseBody
    List<DepartmentDTO> builderTree(@NotNull DepartmentQuery departmentQuery);
}
