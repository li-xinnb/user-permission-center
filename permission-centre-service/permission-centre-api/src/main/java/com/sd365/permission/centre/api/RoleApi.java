/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName RoleApi.java
 * @Author Administrator
 * @Date 2022-9-28  17:26
 * @Description 文件定义了角色的管理接口，包括角色增 删 改查 以及角色分配资源
 * History:
 * <author> Administrator
 * <time> 2022-9-28  17:26
 * <version> 1.0.0
 * <desc> 文件定义了角色的管理接口，包括角色增 删 改查 以及角色分配资源
 */
package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.dto.*;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.RoleCompanyVO;
import com.sd365.permission.centre.pojo.vo.RoleVO;
import io.swagger.annotations.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description RoleController层接口。
 * 主要方法为 角色分配资源（assignResource）和角色分配用户（assignUser）
 * 其他方法较为常规主要是针对角色的增删改查
 * @Class RoleApi
 * @author Gayrot
 * @date 2024/4/22 11:24
 */

@CrossOrigin
@Api(tags = "角色管理 ", value = "/permission/centre/v1/role")
@RequestMapping(value = "/permission/centre/v1/role")
@Validated
public interface RoleApi {

    /**
     * @Description 系统启动时候将初始化角色和资源的关系，主要为网关鉴权服务
     * @author Gayrot
     * @date 2024/4/15 9:10
     * @param role 不同的角色初始化不同的资源到内存
     * @return Boolean 成功则true 否则false
     */
    @ApiOperation(tags = "调用接口初始化角色和资源关系", value = "")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    }
    )
    @PutMapping(value = "/initRoleReourceRedis")
    Boolean initRoleResourceRedis(@ApiParam(name = "role",type = "Role",required = true) @NotNull Role role);


    /**
     * @Description 为角色分配资源 支持选择多个角色统一分配资源
     * @author Gayrot
     * @date 2024/4/12 16:23
     * @param roleResourceDTO 对象包含角色id列表和资源列表 为必须填写
     * @return Boolean 成功则true 否则false 异常则被全局统一异常捕获
     */
    @ApiOperation(tags = "为多角色分配多资源", value = "/assignResource")
    @ApiImplicitParam(name = "roleResourceDTO",value = "角色对应资源对象",required = true,dataType = "RoleResourceDTO",paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    }
    )
    @PostMapping(value = "/assignResource")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    Boolean assignResource(@NotNull @Valid @RequestBody RoleResourceDTO roleResourceDTO);


    /**
     * @Description 为角色分配用户，因为涉及多条插入记得启动事务管理
     * @author Gayrot
     * @date 2024/4/12 16:41
     * @param userRoleDTO 当前勾选的用户以及从用户查询对话框多选的用户
     * @return Boolean 插入成功则true 失败则false
     */
    @ApiOperation(tags = "为多角色分配多用户", value = "/assignUser")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    }
    )
    @PostMapping(value = "/assignUser")
    @ResponseBody
    Boolean assignUser(@NotNull @Valid @RequestBody UserRoleDTO userRoleDTO);

    /**
     * 通过角色id获取该角色拥有的资源 ，数据默认为该租户
     */
    @ApiOperation(tags = "查询角色表中包含的公司", value = "/selectCompany")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    })
    @GetMapping(value = "/selectCompany")
    @ResponseBody
    @Deprecated
    List<Node> selectCompany();

    /**
     * @Description 增加角色,允许不带资源列表，后续用户通过assignRoleResource接口增加资源
     * @author Gayrot
     * @date 2024/4/12 16:04
     * @param roleDTO 角色DTO
     * @return Boolean 成功则true CommonResponse 应答码和消息统一参考基础框架
     */
    @ApiOperation(tags = "增加角色", value = "/add")
    @ApiImplicitParam(name = "roleDTO",value = "增加的角色",required = true,dataType = "RoleDTO",paramType = "body")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    })
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add(@NotNull @Valid @RequestBody  RoleDTO roleDTO);


    /**
     * @Description 删除角色，同时删除角色对应资源
     * @author Gayrot
     * @date 2024/4/13 11:10
     * @param id 角色id
     * @param version 角色记录版本
     * @return Boolean true成功 false失败 如果内部删除主表和明细表失败则引发异常
     */
    @ApiOperation(tags = "删除角色", value = "/remove")
    @ApiImplicitParams({@ApiImplicitParam(name = "id",required =true,dataType = "Long",paramType = "query"),
            @ApiImplicitParam(name = "version",required =true,dataType = "Long",paramType = "query")}
    )
    @DeleteMapping(value = "/remove")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @NotNull @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true)@NotNull @RequestParam("version") Long version);

    /**
     * @Description 批量删除角色，注意要记得删除角色关联的资源
     * @author Gayrot
     * @date 2024/4/12 14:31
     * @param roleDTOS 包括角色id和version
     * @return Boolean 成功则true 否则false 如果批量中一条失败则引发事务回滚
     */
    @ApiOperation(tags = "批量删除角色", value = "/batch")
    @ApiImplicitParams({@ApiImplicitParam(name = "roleDTOS",required =true,dataType = "RoleDTO",paramType = "body")}
    )
    @DeleteMapping(value = "/batch")
    @ResponseBody
    Boolean batchRemove(@NotNull @Valid @RequestBody  RoleDTO[] roleDTOS);

    /**
     * @Description 修改角色的数据，依据前端界面改修改只支持角色的信息不包括角色所有用的资源
     * @author Gayrot
     * @date 2024/4/12 15:01
     * @param roleDTO 角色基本信息 注意必须填字段
     * @return Boolean 成功 true 失败 false 如果触发重复的角色编号等规则可能引发异常全局异常捕获
     */
    @ApiOperation(tags = "修改角色", value = "/update")
    @ApiImplicitParam(name = "roleDTO",value = "角色对象",required = true,dataType = "RoleDTO",paramType = "body")
    @PutMapping(value = "/update")
    @ResponseBody
    Boolean modify(@NotNull @Valid @RequestBody RoleDTO roleDTO);

    /**
     * @Description 执行数据的查询然后在展示在拷贝对话框
     * @author Gayrot
     * @date 2024/4/12 16:10
     * @param id 所选择的拷贝的角色id
     * @return RoleDTO 依据id返回 角色DTO对象
     */
    @ApiOperation(tags = "拷贝角色", value = "/copy")
    @PostMapping(value = "/copy")
    @ResponseBody
    RoleDTO getRoleDtoForCopy(@ApiParam(name = "id",type="String",required = true) @NotNull Long id);

    /**
     * @Description 角色通用分页查询
     * @author Gayrot
     * @date 2024/4/12 16:15
     * @param roleQuery
     * @return CommonPage<RoleDTO>
     */
    @ApiOperation(tags = "查询角色", value = "/commonQuery")
    @ApiImplicitParam(name = "roleQuery",value = "角色查询参数",required = true,dataType = "roleQuery",paramType = "query")
    @GetMapping(value = "/commonQuery")
    @ResponseBody
    CommonPage<RoleDTO> commonQuery(@NotNull RoleQuery roleQuery);


    /**
     * @Description 角色绑定相关用户的时候，需要弹出dialog显示用户数据
     * @author Gayrot
     * @date 2024/4/15 9:13
     * @param userQuery
     * @return CommonPage<UserDTO>
     */
    @ApiOperation(tags = "查询用户", value = "/commonQueryUser")
    @ApiImplicitParam(name = "userQuery",value = "用户查询参数",required = true,dataType = "UserQuery",paramType = "query")
    @GetMapping(value = "/commonQueryUser")
    @ResponseBody
    CommonPage<UserDTO> commonQueryUser(@NotNull UserQuery userQuery);


    /**
     * @Description 用于编辑角色需要展示角色数据，通过角色id返回角色对象
     * @author Gayrot
     * @date 2024/4/15 9:44
     * @param id
     * @return RoleVO
     */
    @ApiOperation(tags = "查询角色 BY ID", value = "/{id}")
    @ApiImplicitParam(name = "id",value = "角色id",required = true,dataType = "Long",paramType = "path")
    @GetMapping(value = "/{id}")
    @ResponseBody
    RoleVO queryRoleById(@NotNull @PathVariable("id") Long id);




    /**
     * @Description 验证角色名字是否已经存在
     * @author Gayrot
     * @date 2024/5/11 14:36
     * @param roleDTO
     * @return boolean
     */
    @ApiOperation(tags = "验证角色是否重复", value = "")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    })
    @PostMapping(value = "/haveName")
    @ResponseBody
    @Deprecated
    boolean haveRole(@NotNull @RequestBody RoleDTO roleDTO);

    /**
     * @Description 查看角色的通过角色id获取该角色拥有的资源,角色界面未发起对该方法考虑非在角色管理模块
     * @author Gayrot
     * @date 2024/4/11 14:36
     * @param roleQuery
     * @return List<Node>
     */
    @ApiOperation(tags = "根据roleId查询角色分配的资源", value = "")
    @ApiResponses({
            @ApiResponse(code = 0,message = "请求成功"),
            @ApiResponse(code=-1,message = "没有更新记录,非0码则代表失败")
    })
    @PostMapping(value = "/resource")
    @ResponseBody
    @Deprecated
    List<Node> doQueryResourceByRoleId(@NotNull @RequestBody RoleQuery roleQuery);

    /**
     * @Description
     * @author Gayrot
     * @date 2024/5/11 14:36
     * @param id
     * @return RoleVO
     */
    @ApiOperation(tags = "查询用户资源 BY ID", value = "")
    @ApiImplicitParam(name = "id",value = "用户id",required = true,dataType = "Long",paramType = "query")
    @GetMapping(value = "/queryUserResource")
    @ResponseBody
    RoleVO queryUserResource(@NotNull @RequestParam(value = "id", required = true) Long id);
}
