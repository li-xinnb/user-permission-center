package com.sd365.permission.centre.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:42
 */

@ApiModel(value="角色资源管理")
@Data
@AllArgsConstructor
public class RoleResourceVO extends TenantBaseVO {
    /**
     * 角色ID
     */
    @ApiModelProperty(value="roleId角色ID")
    private Long roleId;

    /**
     * 资源ID
     */
    @ApiModelProperty(value="resourceId资源ID")
    private Long resourceId;

    /**
     * 预计到达时间
     */
    @ApiModelProperty(value="predictReachTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createdTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value="updateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updatedTime;
}
