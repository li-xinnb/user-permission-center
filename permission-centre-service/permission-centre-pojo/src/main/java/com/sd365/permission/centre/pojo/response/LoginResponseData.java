package com.sd365.permission.centre.pojo.response;

import com.sd365.permission.centre.pojo.vo.ResourceVO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Gayrot
 * @Description
 * @date 2023/06/16 18:58
 **/
@ApiModel(value="用于登录认证返回的应答")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginResponseData {
    /**
     * 生成的token ，token在认证服务生成比较合理
     */
    private String token;
    /**
     *  一个用户可以拥有多个角色这里是id
     */
    private List<String> roles;
    /**
     *  名字
     */
    private String name;
    /**
     * 头像地址
     */
    private String avatar;
    /**
     *  用户所拥有的资源，如果在 Role对象中包含这个会更好
     */
    private List<ResourceVO> resourceVO;
}
