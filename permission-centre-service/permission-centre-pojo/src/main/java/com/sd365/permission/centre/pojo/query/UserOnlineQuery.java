package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:52
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserOnlineQuery extends BaseQuery {
    /**
     * 名字
     */
    private String name;
    /**
     * 工号
     */
    private String code;
}
