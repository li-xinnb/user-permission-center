package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author Gayrot
 * @Description
 * @date 2024/05/11 16:39
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="用户个人信息修改DTO")
public class UpdatePasswordDTO extends TenantBaseDTO {

    @ApiModelProperty(value="旧密码")
    private String oldPassword;

    @ApiModelProperty(value="新密码")
    @NotNull(message = "password密码不能为空")
    private String password;
}
