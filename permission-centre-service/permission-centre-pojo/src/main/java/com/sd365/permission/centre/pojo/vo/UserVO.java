package com.sd365.permission.centre.pojo.vo;


import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;
import java.util.Date;
import java.util.List;



/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:43
 */

@ApiModel(value="用户管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVO extends TenantBaseVO {
    /**
     * 角色用户所拥有的角色 可以通过 ResultMap <collection 指定子查询获取
     */
    @Transient
    private List<RoleVO> roleList;

    /**
     * 工号
     */
    @ApiModelProperty(value="code工号")
    private String code;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 名字
     */
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 头像
     */
    @ApiModelProperty(value="profilePicture头像")
    private String profilePicture;

    /**
     * 性别
     */
    @ApiModelProperty(value="sex性别")
    private Byte sex;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    private Date birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 其他联系
     */
    @ApiModelProperty(value="other其他联系")
    private String other;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 部门ID
     */
    @ApiModelProperty(value="departmentId部门ID")
    private Long departmentId;

    /**
     * 职位ID  注意这个不等同于角色实际就是岗位
     */
    @ApiModelProperty(value="positionId职位ID  注意这个不等同于角色实际就是岗位")
    private Long positionId;

    /**
     * 公司
     */
    @Transient
    private CompanyVO company;

    /**
     * 组织
     */
    @Transient
    private OrganizationVO organization;
    private PositionVO positionVO;
    private DepartmentVO departmentVO;

    /**
     * 工号
     */
    public String getCode() {
        return code;
    }

    /**
     * 工号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 名字
     */
    public String getName() {
        return name;
    }

    /**
     * 名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 头像
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    /**
     * 头像
     */
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    /**
     * 性别
     */
    public Byte getSex() {
        return sex;
    }

    /**
     * 性别
     */
    public void setSex(Byte sex) {
        this.sex = sex;
    }

    /**
     * 生日
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * 生日
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * 电话
     */
    public String getTel() {
        return tel;
    }

    /**
     * 电话
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * 邮箱
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 其他联系
     */
    public String getOther() {
        return other;
    }

    /**
     * 其他联系
     */
    public void setOther(String other) {
        this.other = other;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 部门ID
     */
    public Long getDepartmentId() {
        return departmentId;
    }

    /**
     * 部门ID
     */
    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    /**
     * 职位ID  注意这个不等同于角色实际就是岗位
     */
    public Long getPositionId() {
        return positionId;
    }

    /**
     * 职位ID  注意这个不等同于角色实际就是岗位
     */
    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }
}


