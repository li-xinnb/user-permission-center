package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:48
 * 组织查询类
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationQuery extends BaseQuery {
    /**
     * 名称
     */
    private String name;

    /**
     * 编号
     */
    private String code;

    /**
     * 负责人
     */
    private String master;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 地址
     */
    private String address;
}
