package com.sd365.permission.centre.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 配置文件许可信息
 * @author Gayrot
 * @date 2023/6/12 9:36
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LicensesVO {

    /**
     * 描述
     */
    private String licensesDescription;
    /**
     * 授权机器码
     */
    private String authorizedMachineCode;
    /**
     * 授权产品名称
     */
    private String authorizedProductName;
    /**
     * 授权公司名称
     */
    private String authorizedCompanyName;
    /**
     * 授权访问地址
     */
    private String authorizedAccessAddress;
    /**
     * 系统访问地址
     */
    private String systemAccessAddress;
    /**
     * 当前平台版本
     */
    private String currentPlatformVersion;
    /**
     * 版权
     */
    private String copyRight;

    public String getLicensesDescription() {
        return licensesDescription;
    }

    public void setLicensesDescription(String licensesDescription) {
        this.licensesDescription = licensesDescription;
    }

    public String getAuthorizedMachineCode() {
        return authorizedMachineCode;
    }

    public void setAuthorizedMachineCode(String authorizedMachineCode) {
        this.authorizedMachineCode = authorizedMachineCode;
    }

    public String getAuthorizedProductName() {
        return authorizedProductName;
    }

    public void setAuthorizedProductName(String authorizedProductName) {
        this.authorizedProductName = authorizedProductName;
    }

    public String getAuthorizedCompanyName() {
        return authorizedCompanyName;
    }

    public void setAuthorizedCompanyName(String authorizedCompanyName) {
        this.authorizedCompanyName = authorizedCompanyName;
    }

    public String getAuthorizedAccessAddress() {
        return authorizedAccessAddress;
    }

    public void setAuthorizedAccessAddress(String authorizedAccessAddress) {
        this.authorizedAccessAddress = authorizedAccessAddress;
    }

    public String getSystemAccessAddress() {
        return systemAccessAddress;
    }

    public void setSystemAccessAddress(String systemAccessAddress) {
        this.systemAccessAddress = systemAccessAddress;
    }

    public String getCurrentPlatformVersion() {
        return currentPlatformVersion;
    }

    public void setCurrentPlatformVersion(String currentPlatformVersion) {
        this.currentPlatformVersion = currentPlatformVersion;
    }

    public String getCopyRight() {
        return copyRight;
    }

    public void setCopyRight(String copyRight) {
        this.copyRight = copyRight;
    }
}

