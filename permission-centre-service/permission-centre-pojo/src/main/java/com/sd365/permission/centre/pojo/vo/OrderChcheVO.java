package com.sd365.permission.centre.pojo.vo;

import lombok.Data;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:39
 * 缓存指令
 */

@Data
public class OrderChcheVO {
    private String name;
    private String totalNum;
}
