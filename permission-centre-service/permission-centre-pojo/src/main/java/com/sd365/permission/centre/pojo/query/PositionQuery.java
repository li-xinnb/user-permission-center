package com.sd365.permission.centre.pojo.query;


import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:48
 * 用户的搜索
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositionQuery extends TenantBaseQuery {



    private Long id;//职位id
    private String name;//职位名称
    private String code;//职位编号

    private Byte status;//状态

}
