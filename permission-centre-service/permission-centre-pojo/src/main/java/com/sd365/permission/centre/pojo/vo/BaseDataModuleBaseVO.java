package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.dto.OrganizationDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:22
 */

@Data
@AllArgsConstructor
public abstract class BaseDataModuleBaseVO extends TenantBaseVO {

    /**
     *  VO 需要此数据
     */
    private OrganizationDTO organizationDTO;
    /**
     *  VO 需要次数据
     */
    private CompanyDTO companyDTO;


    public BaseDataModuleBaseVO() {
        this.organizationDTO=new OrganizationDTO();
        this.companyDTO=new CompanyDTO();

    }
}
