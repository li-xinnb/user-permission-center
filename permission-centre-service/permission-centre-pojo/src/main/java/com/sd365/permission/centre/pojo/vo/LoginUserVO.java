package com.sd365.permission.centre.pojo.vo;

import com.sd365.permission.centre.pojo.response.LoginResponseData;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author Gayrot
 * @Description
 * @date 2023/06/16 18:55
 * 用于在登录认证中返回角色资源信息
 **/
@ApiModel(value="用于登录认证返回的UserVO")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginUserVO {
    /**
     * 名字
     */
    private String name;
    /**
     * 电话号码
     */
    private String tel;
    /**
     * 主键雪花算法生成
     */
    private Long id;
    /**
     * 消息和错误码
     */
    private String msg;
    private int code;

    private Long tenantId;
    private String companyAddress;
    private Long orgId;
    private Long companyId;
    public LoginResponseData data;
    private List<Long> roleIds;
}
