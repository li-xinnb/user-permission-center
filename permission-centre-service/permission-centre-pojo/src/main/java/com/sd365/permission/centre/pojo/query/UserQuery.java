package com.sd365.permission.centre.pojo.query;


import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:55
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserQuery extends TenantBaseQuery {
    /**
     * 名字
     */
    private String name;
    /**
     * 工号
     */
    private String code;
    /**
     * 电话
     */
    private String tel;
    /**
     * 部门
     */
    private Long departmentId;
    /**
     * 职位
     */
    private Long positionId;
    /**
     * 角色
     */
    private Long roleId;


}
