package com.sd365.permission.centre.pojo.dto.pcp;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Table;
import java.util.Date;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:20
 */

@ApiModel(value="com.sd365.pcp.omp.basedata.dao.entity.User")
@Table(name = "op_basic_user")
@Data
public class PcpUserDTO extends TenantBaseDTO {
    private String tel;
    private String password;
    /**
     * 账户
     */
    @ApiModelProperty(value="acount账户")
    private String acount;

    /**
     * 姓名
     */
    @ApiModelProperty(value="name姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty(value="sex性别")
    private Byte sex;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    private Date birthday;

    /**
     * 账户金额
     */
    @ApiModelProperty(value="acountPrice账户金额")
    private Integer acountPrice;

    /**
     * 等级
     */
    @ApiModelProperty(value="level等级")
    private Byte level;

    /**
     * 微信的openid
     */
    @ApiModelProperty(value="openId微信的openid")
    private String openId;

    /**
     * 账户
     */
    public String getAcount() {
        return acount;
    }

    /**
     * 账户
     */
    public void setAcount(String acount) {
        this.acount = acount;
    }

    /**
     * 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 性别
     */
    public Byte getSex() {
        return sex;
    }

    /**
     * 性别
     */
    public void setSex(Byte sex) {
        this.sex = sex;
    }

    /**
     * 生日
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * 生日
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * 账户金额
     */
    public Integer getAcountPrice() {
        return acountPrice;
    }

    /**
     * 账户金额
     */
    public void setAcountPrice(Integer acountPrice) {
        this.acountPrice = acountPrice;
    }

    /**
     * 等级
     */
    public Byte getLevel() {
        return level;
    }

    /**
     * 等级
     */
    public void setLevel(Byte level) {
        this.level = level;
    }

    /**
     * 微信的openid
     */
    public String getOpenId() {
        return openId;
    }

    /**
     * 微信的openid
     */
    public void setOpenId(String openId) {
        this.openId = openId;
    }
}