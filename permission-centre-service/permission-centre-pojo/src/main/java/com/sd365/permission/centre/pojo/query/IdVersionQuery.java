package com.sd365.permission.centre.pojo.query;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:46
 * 多用于删除
 */

public class IdVersionQuery {
    private Long Id;

    private Long version;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }
}
