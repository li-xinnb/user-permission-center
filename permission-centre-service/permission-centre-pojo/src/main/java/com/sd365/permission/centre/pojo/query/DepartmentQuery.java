package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:45
 */

@Data
public class DepartmentQuery extends BaseQuery {
    private Long id;
    private  Long companyId;
    /**
     * 部门名称
     */
    @ApiModelProperty(value = "name部门名称")
    private String name;

    /**
     * 助记码
     */
    @ApiModelProperty(value = "mnemonicCode助记码")
    private String mnemonicCode;

    /**
     * 部门编号
     */
    @ApiModelProperty(value = "code部门编号")
    private String code;

    /**
     * 部门级别
     */
    @ApiModelProperty(value = "level部门级别")
    private String level;

    /**
     * 上级部门
     */
    @ApiModelProperty(value = "parentId上级部门")
    private Long parentId;

    /**
     * 负责人
     */
    @ApiModelProperty(value = "master负责人")
    private String master;

    /**
     * 部门描述
     */
    @ApiModelProperty(value = "descript部门描述")
    private String descript;

    //部门状态
    @ApiModelProperty(value = "status部门状态")
    private Byte status;

    /**
     * 部门名称
     */
    public String getName() {
        return name;
    }

    /**
     * 部门名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 助记码
     */
    public String getMnemonicCode() {
        return mnemonicCode;
    }

    /**
     * 助记码
     */
    public void setMnemonicCode(String mnemonicCode) {
        this.mnemonicCode = mnemonicCode;
    }

    /**
     * 部门编号
     */
    public String getCode() {
        return code;
    }

    /**
     * 部门编号
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 部门级别
     */
    public String getLevel() {
        return level;
    }

    /**
     * 部门级别
     */
    public void setLevel(String level) {
        this.level = level;
    }

    /**
     * 上级部门
     */
    public Long getParentId() {
        return parentId;
    }

    /**
     * 上级部门
     */
    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    /**
     * 负责人
     */
    public String getMaster() {
        return master;
    }

    /**
     * 负责人
     */
    public void setMaster(String master) {
        this.master = master;
    }

    /**
     * 部门描述
     */
    public String getDescript() {
        return descript;
    }

    /**
     * 部门描述
     */
    public void setDescript(String descript) {
        this.descript = descript;
    }
}