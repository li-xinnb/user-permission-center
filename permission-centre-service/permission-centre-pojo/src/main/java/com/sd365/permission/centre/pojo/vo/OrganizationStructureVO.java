package com.sd365.permission.centre.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:40
 * 架构下的公司以及公司下的部门信息
 */

@Data
public class OrganizationStructureVO {
    private String label;
    private Long id;
    private List<OrganizationStructureVO> children;
}
