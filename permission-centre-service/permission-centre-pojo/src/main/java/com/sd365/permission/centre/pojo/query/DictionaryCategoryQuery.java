package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:46
 * 用于查询自动分页
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
public class DictionaryCategoryQuery extends TenantBaseQuery {
    /**
     * 名称
     */
    private String name;
    /**
     * 备注
     */
    private String remark;
}
