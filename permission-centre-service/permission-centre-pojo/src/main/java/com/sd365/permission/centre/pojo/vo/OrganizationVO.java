package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:41
 * 组织VO
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationVO extends BaseEntity {
    /**
     * 名称
     */
    private String name;

    /**
     * 编号
     */
    private String code;

    /**
     * 负责人
     */
    private String master;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 地址
     */
    private String address;
}

