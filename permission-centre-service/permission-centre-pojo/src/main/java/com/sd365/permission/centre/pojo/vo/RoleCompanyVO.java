package com.sd365.permission.centre.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:41
 */

@ApiModel(value="角色公司管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleCompanyVO {
    /**
     * 角色ID
     */
    @ApiModelProperty(value = "roleId角色ID")
    private Long roleId;

    /**
     * 授权公司ID
     */
    @ApiModelProperty(value = "authCompanyIds授权公司ID列表")
    private List<Long> authCompanyIds;
}
