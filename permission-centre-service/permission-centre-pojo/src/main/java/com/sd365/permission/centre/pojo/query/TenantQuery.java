package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:52
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantQuery extends TenantBaseQuery {
    /**
     * 租户名
     */
    private String name;

    /**
     * 租户账号
     */
    private String account;

    private Byte status;
}
