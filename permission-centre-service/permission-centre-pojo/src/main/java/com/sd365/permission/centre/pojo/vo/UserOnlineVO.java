package com.sd365.permission.centre.pojo.vo;


import com.sd365.common.core.common.pojo.vo.BaseVO;
import com.sd365.permission.centre.pojo.dto.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Transient;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:43
 */

@ApiModel(value="用户在线管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserOnlineVO extends BaseVO {

    /**
     * 工号
     */
    @ApiModelProperty(value="code工号")
    private String code;


    /**
     * 名字
     */
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 公司
     */
    @Transient
    private CompanyDTO company;

    /**
     * 租户
     */
    @Transient
    private TenantDTO tenant;

    /**
     * 组织
     */
    @Transient
    private OrganizationDTO organization;
    private PositionDTO position;
    private DepartmentDTO department;

    /**
     * 主机地址
     */
    @ApiModelProperty(value="ip地址")
    private String ipaddr;

    /**
     * 浏览器名称
     */
    @ApiModelProperty(value="浏览器名称")
    private String browser;

    /**
     * 登陆系统
     */
    @ApiModelProperty(value="登陆系统")
    private String os;

    /**
     * 登陆时间
     */
    @ApiModelProperty(value="登陆时间")
    private String loginTime;
}


