package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.BaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:19
 */
@ApiModel(value="区域管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdministrativeVO extends BaseVO {
    /**
     * 区域代码
     */
    @ApiModelProperty(value="code区域代码")
    private Long code;

    @ApiModelProperty(value="mnemonicCode助记码")
    private String mnemonicCode;

    /**
     * 区域名称
     */
    @ApiModelProperty(value="name区域名称")
    private String name;

    /**
     * 区域类型
     */
    @ApiModelProperty(value="areaType类型")
    private String areaType;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 排序号
     */
    @ApiModelProperty(value="orderNum排序号")
    private Long orderNum;
    /**
     * 组织ID
     */
    @ApiModelProperty(value="orgId组织id")
    private Long orgId;

    /**
     * 租户ID
     */
    @ApiModelProperty(value="tenantId租户id")
    private Long tenantId;

    /**
     * 上级区域区域id
     */
    @ApiModelProperty(value="parentId上级区域id")
    private Long parentId;
    /**
     * 修改者
     */
    @ApiModelProperty(value="modifier修改者")
    private String modifier;
    /**
     * 创建者
     */
    @ApiModelProperty(value="creator创建者")
    private String creator;
    /**
     * 上级区域
     */
    private AdministrativeVO upAdministrative;
}
