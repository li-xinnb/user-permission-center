package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Table;

/**
 * 加入公司名和机构名，用于传输数据到前端
 * @author Gayrot
 * @date 2023/6/12 9:25
 */

@Table(name = "basic_dictionary_category")
@Data
@EqualsAndHashCode(callSuper = true)
public class DictionaryCategoryVO extends TenantBaseVO {
    private String name;
    private String remark;
}
