package com.sd365.permission.centre.pojo.dto.easy2pass;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:21
 */

@ApiModel(value = "com.sd365.passcode.dao.entity.ApplicationUser")
@Table(name = "application_user")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ApplicationUserDTO extends TenantBaseDTO {
    private String password;
    private String address;
    private String unit;
    /**
     * 微信用户的openid
     */
    @ApiModelProperty(value = "wxOpenId微信用户的openid")
    @NotNull
    private String wxOpenId;

    /**
     * 图片地址
     */
    @ApiModelProperty(value = "imageUrl图片地址")
    private String imageUrl;

    /**
     * 电话
     */
    @NotNull
    @ApiModelProperty(value = "tel电话")
    private String tel;

    /**
     * 性别
     */
    @NotNull
    @ApiModelProperty(value = "sex性别")
    private Byte sex;

    /**
     * 账户
     */
    @NotNull
    @ApiModelProperty(value = "account账户")
    private String account;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "name姓名")
    private String name;

    /**
     * 等级
     */
    @ApiModelProperty(value = "grade等级")
    private Integer grade;

    /**
     * 积分
     */
    @ApiModelProperty(value = "integral积分")
    private Integer integral;

    /**
     * 身份证
     */
    @ApiModelProperty(value = "idCard身份证")
    private String idCard;

    /**
     * 生日
     */
    @ApiModelProperty(value = "birthday生日")
    private Date birthday;

    /**
     * 邮箱
     */

    @ApiModelProperty(value = "eMail邮箱")
    private String eMail;

    /**
     * 职业
     */
    @ApiModelProperty(value = "occupation职业")
    private String occupation;


}