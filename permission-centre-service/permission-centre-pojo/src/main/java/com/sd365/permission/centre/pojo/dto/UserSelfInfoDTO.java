package com.sd365.permission.centre.pojo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author Gayrot
 * @Description
 * @date 2024/05/11 18:48
 **/
@Data
@AllArgsConstructor
@ApiModel(value = "UserSelfInfoDTO")
public class UserSelfInfoDTO extends TenantBaseDTO {
    /**
     * 名字
     */
    @NotNull(message = "name名字不能为空")
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    @JsonFormat(locale = "zh",timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    private Date birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    @NotNull(message = "email邮箱不能为空")
    private String email;

    /**
     * 其他联系
     */
    @ApiModelProperty(value="other其他联系")
    private String other;
}
