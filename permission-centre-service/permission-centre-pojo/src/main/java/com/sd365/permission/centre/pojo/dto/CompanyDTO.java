package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @Description
 * @author Gayrot
 * @date 2023/6/12 10:07
 */

@Data
@AllArgsConstructor
public class CompanyDTO extends BaseDTO {
    /**
     * 公司名
     */
    @ApiModelProperty(value="name公司名")
    @NotNull(message = "名称不能为空")
    private String name;

    /**
     * 公司编号
     */
    @ApiModelProperty(value="code公司编号")
    @NotNull(message = "编号不能为空")
    private String code;

    /**
     * 助记码
     */
    @ApiModelProperty(value="mnemonicCode助记码")
    @NotNull(message = "助记码不能为空")
    private String mnemonicCode;

    /**
     * 法人
     */
    @ApiModelProperty(value="master法人")
    @NotNull(message = "法人不能为空")
    private String master;

    /**
     * 税号
     */
    @ApiModelProperty(value="tax税号")
    @NotNull(message = "税号不能为空")
    private String tax;

    /**
     * 传真
     */
    @ApiModelProperty(value="fax传真")
    private String fax;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 地址
     */
    @ApiModelProperty(value="address地址")
    private String address;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;
    /**
     * 网址
     */
    @ApiModelProperty(value="website网址")
    private String website;

    @ApiModelProperty(value="orgId组织")
    private Long orgId;

    @ApiModelProperty(value="tenantId租户")
    private Long tenantId;

    @ApiModelProperty(value="修改人")
    private String modifier;

    private OrganizationDTO organizationDTO;

    private TenantDTO tenantDTO;

    public CompanyDTO() {
        organizationDTO = new OrganizationDTO();
        tenantDTO = new TenantDTO();
    }


}
