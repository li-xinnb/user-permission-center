package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:43
 */

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantVO extends TenantBaseVO {
    /**
     * 在租赁平台注册的账号
     */
    @ApiModelProperty(value="account在租赁平台注册的账号")
    private String account;

    /**
     * 密码
     */
    @ApiModelProperty(value="password密码")
    private String password;

    /**
     * 名字
     */
    @ApiModelProperty(value="name名字")
    private String name;

    /**
     * 头像
     */
    @ApiModelProperty(value="profilePicture头像")
    private String profilePicture;

    /**
     * 性别
     */
    @ApiModelProperty(value="sex性别")
    private Byte sex;

    /**
     * 生日
     */
    @ApiModelProperty(value="birthday生日")
    private Date birthday;

    /**
     * 电话
     */
    @ApiModelProperty(value="tel电话")
    private String tel;

    /**
     * 邮箱
     */
    @ApiModelProperty(value="email邮箱")
    private String email;

    /**
     * 其他联系
     */
    @ApiModelProperty(value="other其他联系")
    private String other;

    /**
     * 租户属性  0个人用户  1企业用户
     */
    @ApiModelProperty(value="tenantAttr租户属性  0个人用户  1企业用户")
    private Byte tenantAttr;

    /**
     * 租户单位名称  查询条件
     */
    @ApiModelProperty(value="orgName租户单位名称  查询条件")
    private String orgName;

    /**
     * 有效开始时间
     */
    @ApiModelProperty(value="availableTime有效开始时间")
    private Date availableTime;

    /**
     * 过期时间
     */
    @ApiModelProperty(value="expireTime过期时间")
    private Date expireTime;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     *
     */
    @ApiModelProperty(value="creator")
    private String creator;

    /**
     *
     */
    @ApiModelProperty(value="modifier")
    private String modifier;
}
