package com.sd365.permission.centre.pojo.vo;

import lombok.Data;

/**
 * 缓存监控 key
 * @author Gayrot
 * @date 2023/6/12 9:36
 */

@Data
public class KeyValueVo {
    private String key;
    private Object value;
}
