package com.sd365.permission.centre.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.dto.OrganizationDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Gayrot
 * @date 2023/6/12 9:42
 */

@ApiModel(value="角色管理")
@Data
@AllArgsConstructor
public class RoleVO extends TenantBaseVO {
    private List<ResourceVO> resourceVOS=new ArrayList<>();
    /**
     * 角色名
     */
    @ApiModelProperty(value="name角色名")
    private String name;

    /**
     * 角色代码
     */
    @ApiModelProperty(value="code角色代码")
    private String code;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /*
     * 预计到达时间
     */
    @ApiModelProperty(value="predictReachTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createdTime;

    /**
     * 修改时间
     */
    @ApiModelProperty(value="updateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date updatedTime;

    /**
     * 所属公司名
     */
    private String companyName;
    /**
     * 机构名
     */
    private String organizationName;
    /**
     * 结算类型
     */
    private String tenantName;

    private CompanyDTO companyDTO;
    private OrganizationDTO organizationDTO;
    private TenantDTO tenantDTO;

    public RoleVO() {
        companyDTO = new CompanyDTO();
        organizationDTO = new OrganizationDTO();
        tenantDTO = new TenantDTO();
    }
}

