package com.sd365;

import com.aliyuncs.utils.StringUtils;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.permission.centre.entity.RoleResource;
import com.sd365.permission.centre.service.util.Md5Utils;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.springframework.util.ObjectUtils;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println(cover("妈妈"));
    }

    public static String cover(String chineseText){
        StringBuilder result = new StringBuilder();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        try {
            for (char c : chineseText.toCharArray()) {
                String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c, format);
                if (pinyinArray != null && pinyinArray.length > 0) {
                    // 将拼音数组的第一个拼音添加到结果中
                    result.append(pinyinArray[0]).append(" ");
                } else {
                    // 如果当前字符不是汉字，则直接添加到结果中
                    result.append(c).append(" ");
                }
            }
            return result.toString().trim(); // 去除末尾的空格并返回结果
        }catch (Exception e){
            e.printStackTrace();
        }
        return chineseText;
    }

}
