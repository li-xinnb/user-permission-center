package com.sd365.permission.centre.service.util;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;

/**
 * @author Gayrot
 * @Description 中文转英文字母
 * @date 2024/04/18 19:20
 **/
public class CoverChineseToEnglish {

    public static String cover(String chineseText) {
        StringBuilder result = new StringBuilder();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
        try {
            for (char c : chineseText.toCharArray()) {
                String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c, format);
                if (pinyinArray != null && pinyinArray.length > 0) {
                    // 将拼音数组的第一个拼音添加到结果中
                    result.append(pinyinArray[0]).append(" ");
                } else {
                    // 如果当前字符不是汉字，则直接添加到结果中
                    result.append(c).append(" ");
                }
            }
            // 去除末尾的空格并返回结果
            return result.toString().trim();
        }catch (Exception e){
            e.printStackTrace();
        }
        return chineseText;
    }
}
