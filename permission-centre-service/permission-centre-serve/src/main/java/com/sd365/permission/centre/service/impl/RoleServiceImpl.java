/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName RoleServiceImpl.java
 * @Author Administrator
 * @Date 2022-9-30  16:25
 * @Description 包含角色管理界面的功能实现
 * History:
 * <author> Administrator
 * <time> 2022-9-30  16:25
 * <version> 1.0.0
 * <desc> abel.zhan 优化代码结构 分配用户方法采用清除用户角色重新分配，优化角色的多资源分配方式 优化代码结构和效率改为批量插入
 */
package com.sd365.permission.centre.service.impl;

import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.common.core.common.constant.EntityConsts;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BizErrorCode;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.common.core.common.service.AbstractBusinessService;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.*;
import com.sd365.permission.centre.entity.*;
import com.sd365.permission.centre.pojo.dto.*;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.RoleCompanyVO;
import com.sd365.permission.centre.pojo.vo.RoleVO;
import com.sd365.permission.centre.service.RoleService;
import com.sd365.permission.centre.service.cache.IRoleCache;
import com.sd365.permission.centre.service.exception.UserCentreExceptionCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description
 * @author Gayrot
 * @date 2024/4/11 14:33
 * @param null

 */
@Service
@Slf4j
public class RoleServiceImpl extends AbstractBusinessService implements RoleService {
    /**
     * 角色缓存操作类
     */
    @Autowired
    private IRoleCache roleCache;

    /**
     * redis 操作类用于将角色的资源存储到redis
     */
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;

    /**
     *  角色mapper
     */
    @Autowired
    private RoleMapper roleMapper;

    /**
     *  角色资源mapper 对应basic_role_resource表
     */
    @Autowired
    private RoleResourceMapper roleResourceMapper;

    /**
     *  角色所处公司查询，一个角色可以属于多个公司
     */
    @Autowired
    private RoleCompanyMapper roleCompanyMapper;
    /**
     * 用户角色查询 对应 basic_user_role表
     */
    @Autowired
    private UserRoleMapper userRoleMapper;

    /**
     *  用户mapper
     */
    @Autowired
    private UserMapper userMapper;

    /**
     * id 生成器
     */
    @Autowired
    private IdGenerator idGenerator;

    /**
     * 资源mapper
     */
    @Autowired
    private ResourceMapper resourceMapper;

    /**
     * 职位mapper
     */
    @Autowired
    private PositionMapper positionMapper;

    /**
     * 公司mapper
     */
    @Autowired
    private CompanyMapper companyMapper;

    /**
     * 部门mapper
     */
    @Autowired
    private DepartmentMapper departmentMapper;

    private final static String ROLE_ID = "roleId";

    @Override
    public Boolean loadRoleResourceCache(@NotNull  Role role) {
        List<Role> roles = roleMapper.commonQuery(role);
        if (CollectionUtils.isEmpty(roles)){ throw new BusinessException(BizErrorCode.DATA_SEARCH_NOT_FOUND,"没有找到对应的角色");}
        for (Role role1 : roles) {
            // 查询获得 resourceList
            Example example = new Example(RoleResource.class);
            example.createCriteria().andEqualTo(ROLE_ID,role1.getId());
            // 查询某一个角色的资源列表，这里查询会有点慢好在角色数量不多 考虑优化where in 查询
            List<RoleResource> roleResourceList = roleResourceMapper.selectByExample(example);
            if(CollectionUtils.isEmpty(roleResourceList)){
                continue;
            }
            /**
             * 每个角色 定义为一个 hashmap对象放在 redis ， hash对象在内存中的key为
             * Global.HASH_ROLE_ID_KEY+role1.getId(),  hashmap 的key为 RoleResource的id
             * value为 RoleResource对象
             */
            roleCache.cacheRoleResource(role1,roleResourceList);
        }
        return Boolean.TRUE;
    }

    @Transactional
    @Override
    public Boolean assignResource(@NotNull @Valid RoleResourceDTO roleResourceDTO) {
        // 给多个角色分配多个资源
        List<Long> roleIds = roleResourceDTO.getRoleIds();
        List<Long> resourceIds = roleResourceDTO.getResourceIds();
        // 删除原来的角色-资源
        removeRolesResources(roleIds);
        // 批量插入角色-资源记录
        boolean result = batchInsertRolesResources(roleIds,resourceIds);

        if(result){
            // 开始重置缓存
            List<Role> roleList = new ArrayList<>();
            roleIds.stream().forEach(id->{
                Role role = new Role();
                role.setId(id);
                roleList.add(role);
            });
            roleCache.cachedMultiRolesResources(roleList);
        }
        return result;
    }

    @Override
    public Boolean assignUser(@NotNull @Valid UserRoleDTO userRoleDTO) {
        //给多个用户分配角色
        List<Long> roleIds = userRoleDTO.getRoleIds();
        List<Long> userIds = userRoleDTO.getUserIds();

        //前端传过来的值目前有问题，先去重
        roleIds = roleIds.stream().distinct().collect(Collectors.toList());
        userIds = userIds.stream().distinct().collect(Collectors.toList());
        /**
         * 删除角色-用户记录
         * 假设为角色A,B,C分配用户1,2,3，此时，用户4,5也拥有角色A，用户1拥有角色A,D，用户2拥有角色B,E
         * 因此，我们需要先删除 1-A 1-D 2-B 这几条记录，而保留4-A和5-A，
         * 同时插入1-A,1-B,1-C,2-A,2-B,2-C,3-A,3-B,3-C这些记录
         */
        UserRole deleteUserRole = new UserRole();
        for (Long roleId : roleIds) {
            if(ObjectUtils.isEmpty(roleId)){
                throw new BusinessException(UserCentreExceptionCode.BUSINESS_ROLE_ID_IS_EMPTY,new Exception("分配失败"));
            }
            deleteUserRole.setRoleId(roleId);
            if(userIds.isEmpty()){
                //用户列表为空数组，说明操作者想清除这些角色之前分配过的用户，匹配角色id删除所有记录
                userRoleMapper.delete(deleteUserRole);
            }else {
                //用户列表不为空数组，说明只是想分配新用户，不能动老用户拥有的这个角色，匹配用户id删
                for (Long userId : userIds) {
                    deleteUserRole.setUserId(userId);
                    userRoleMapper.delete(deleteUserRole);
                }
            }
        }

        //如果用户列表为空列表，说明操作者想清除这些角色之前分配过的用户，删完即可，不用插入数据，直接返回跑路
        if(userIds.isEmpty()){
            return true;
        }

        // 插入新的角色-用户记录
        UserRole userRole = new UserRole();
        int insertRows = 0;
        for (Long roleId : roleIds) {
            userRole.setRoleId(roleId);
            // 逐个插入角色-用户，用户不多此操作无大影响
            for (Long userId : userIds) {
                userRole.setId(idGenerator.snowflakeId());
                userRole.setUserId(userId);
                userRole.setStatus(INITIALIZE_STATUS_VALUE);
                userRole.setVersion(INITIALIZE_VERSION_VALUE);
                // 公用字段补充未来直接myabtis插件填写
                super.baseDataStuff4Add(userRole);
                super.baseDataStuffCommonPros(userRole);
                insertRows += userRoleMapper.insert(userRole);
                if(insertRows <= 0){
                    throw new BusinessException(UserCentreExceptionCode.UNKNOWN_ERROR,new Exception("分配失败"));
                }
            }
        }
        return insertRows > 0;
    }

    @Override
    public List<Node> queryResourceByRoleId(@NotNull long roleId) {

        return roleResourceMapper.queryResourceByRoleId(roleId);
    }

    @Override
    @Deprecated
    public List<Node> selectCompany() {
        return roleMapper.selectCompany();
    }

    @Override
    public boolean haveRole(@NotNull @Valid RoleDTO roleDTO) {
        Role role = BeanUtil.copy(roleDTO, Role.class);
        return roleMapper.haveRole(role) > 0;
    }

    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    public Boolean add(@NotNull @Valid RoleDTO roleDTO) {
        Role role = BeanUtil.copy(roleDTO, Role.class);

        //判定code是否已经存在
        Role hasExistRole = roleMapper.getRoleByCode(role);
        if(hasExistRole != null){
            throw new BusinessException(UserCentreExceptionCode.BUSINESS_ROLE_CODE_HAS_EXIST,new Exception("该编号已存在"));
        }

        role.setId(idGenerator.snowflakeId());

        //为了方便测试删除而批量生成角色服务
        BaseContextHolder.set("TESTID",role.getId());

        return roleMapper.insert(role) > 0;
    }

    @Override
    public Boolean remove(@NotNull Long id, @NotNull Long version) {
        //判定角色是否在被使用
        Role role = roleMapper.selectByPrimaryKey(id);
        if(role == null){
            throw new BusinessException(UserCentreExceptionCode.ROLE_NOT_EXIST,new Exception("删除失败"));
        }
        if(role.getStatus() == 1){
            throw new BusinessException(UserCentreExceptionCode.BUSINESS_ROLE_IS_USING,new Exception("删除失败"));
        }

        Example roleExample = new Example(Role.class);
        roleExample.createCriteria().andEqualTo("id", id);

        Example roleResourceExample = new Example(RoleResource.class);
        roleResourceExample.createCriteria().andEqualTo(ROLE_ID, id);

        Example roleUserExample = new Example(UserRole.class);
        roleUserExample.createCriteria().andEqualTo(ROLE_ID, id);

        Example roleCompanyExample = new Example(RoleCompany.class);
        roleCompanyExample.createCriteria().andEqualTo(ROLE_ID,id);

        //删除角色-资源记录
        roleResourceMapper.deleteByExample(roleResourceExample);

        //删除用户-角色记录
        userRoleMapper.deleteByExample(roleUserExample);

        //删除角色-公司记录
        roleCompanyMapper.deleteByExample(roleCompanyExample);

        //删除角色
        boolean result = roleMapper.deleteByExample(roleExample) > 0;
        if(result){
          // 移除缓存中的角色和资源管理
          roleCache.removeCacheRoleResources(id);
        }else {
            throw new BusinessException(UserCentreExceptionCode.BUSSINESS_RECORD_VERSION_ERROR_OR_ROLE_NOT_EXIST,
                    new Exception("数据版本不一致或者该角色不存在"));
        }
        return result;
    }


    @Override
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    public RoleDTO modify(@NotNull @Valid RoleDTO roleDTO) {
        Role role = BeanUtil.copy(roleDTO, Role.class);

        //判定code是否已经存在
        Role hasExistRole = roleMapper.getRoleByCode(role);
        if(hasExistRole != null){
            throw new BusinessException(UserCentreExceptionCode.BUSINESS_ROLE_CODE_HAS_EXIST,new Exception("该编号已存在"));
        }

        int result = roleMapper.updateByPrimaryKey(role);
        if (result > 0) {
            RoleDTO roleDTOCache = BeanUtil.copy(role, RoleDTO.class);
            BeanUtil.copy(role.getCompany(), roleDTOCache.getCompanyDTO(), CompanyDTO.class);
            BeanUtil.copy(role.getOrganization(), roleDTOCache.getOrganizationDTO(), OrganizationDTO.class);
            BeanUtil.copy(role.getTenant(), roleDTOCache.getTenantDTO(), TenantDTO.class);
            return roleDTOCache;
        }else {
            throw new BusinessException(UserCentreExceptionCode.BUSINESS_RECODE_VERSION_NOT_SAME_EXCEPTION,
                    new Exception("数据版本不一致，更新出错，请您稍后再试"));
        }
    }


    @Pagination
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    @Override
    public CommonPage<RoleDTO> commonQuery(@NotNull RoleQuery roleQuery) {

        Role role = new Role();
        BeanUtil.copy(roleQuery,role);
        Page<Role> roles = (Page<Role>) roleMapper.commonQuery(role);
        //对象转化
        List<RoleDTO> roleDTOList = BeanUtil.copyList(roles, RoleDTO.class, (o, o1) -> {
            // 调用 BeanUtil.copyProperties
            if (o == null || o1 == null) {
                throw new BeanException("拷贝对象不可以为空", new Exception("copyList 拷贝回调错误"));
            }

            Role role1 = (Role) o;
            RoleDTO roleDTO = (RoleDTO) o1;
            BeanUtil.copy(role1.getCompany(), roleDTO.getCompanyDTO(), CompanyDTO.class);
            BeanUtil.copy(role1.getOrganization(), roleDTO.getOrganizationDTO(), OrganizationDTO.class);
            BeanUtil.copy(role1.getTenant(), roleDTO.getTenantDTO(), TenantDTO.class);
        });

        return cast2CommonPage(roles,roleDTOList);
    }

    @Override
    public RoleVO queryById(@NotNull Long id) {
        Role role = roleMapper.selectById(id);
        return role != null ? BeanUtil.copy(role, RoleVO.class) : new RoleVO();
    }

    @Override
    public RoleDTO getRoleDtoForCopy(@NotNull Long id) {
        Role role = roleMapper.selectByPrimaryKey(id);
        return role != null ? BeanUtil.copy(role, RoleDTO.class) : new RoleDTO();
    }

    @CommonFieldStuffer(methodType = MethodTypeEnum.DELETE)
    @Transactional
    @Override
    public Boolean batchRemove(@NotEmpty @Valid RoleDTO[] roleDTOS) {
        //更新数据库
        for (RoleDTO roleDTO : roleDTOS) {
            if(ObjectUtils.isEmpty(roleDTO.getId())){
                throw new BusinessException(UserCentreExceptionCode.BUSINESS_ROLE_ID_IS_EMPTY,
                        new Exception("id为空，删除失败！"));
            }
            if(roleDTO.getStatus() == 1){
                throw new BusinessException(UserCentreExceptionCode.BUSINESS_ROLE_IS_USING,new Exception("删除失败"));
            }
            //逐条删除角色-资源记录
            Example roleResourceExample = new Example(RoleResource.class);
            roleResourceExample.createCriteria().andEqualTo(ROLE_ID, roleDTO.getId());
            roleResourceMapper.deleteByExample(roleResourceExample);

            //删除角色-用户记录
            Example userRoleExample = new Example(UserRole.class);
            userRoleExample.createCriteria().andEqualTo(ROLE_ID, roleDTO.getId());
            userRoleMapper.deleteByExample(userRoleExample);

            //删除角色-公司记录
            Example roleCompanyExample = new Example(RoleCompany.class);
            roleCompanyExample.createCriteria().andEqualTo(ROLE_ID,roleDTO.getId());
            roleCompanyMapper.deleteByExample(roleCompanyExample);
        }
        //更新缓存以及角色数据表
        for (RoleDTO roleDTO : roleDTOS) {
            //删除角色记录
            Example roleExample = new Example(Role.class);
            roleExample.createCriteria().andEqualTo("id", roleDTO.getId()).andEqualTo("version", roleDTO.getVersion());
            int result = roleMapper.deleteByExample(roleExample);
            if(result <= 0){
                throw new BusinessException(UserCentreExceptionCode.BUSINESS_RECODE_VERSION_NOT_SAME_EXCEPTION, new Exception("数据版本不一致"));
            }
            // 移除 role的对应资源缓存
            roleCache.removeCacheRoleResources(roleDTO.getId());
        }

        return true;
    }


    @Pagination
    @Override
    public CommonPage<UserDTO> commonQueryUserForRoleBindingUsers(@NotNull UserQuery userQuery) {

        Example example = new Example(User.class);
        example.setOrderByClause("updated_time DESC");
        Example.Criteria criteria = example.createCriteria();

        if (userQuery.getName() != null) {
            //根据name模糊查询
            criteria.orLike("name", "%" + userQuery.getName() + "%");
        } else {
            criteria.orLike("name", userQuery.getName());
        }

        Page<User> users = (Page)userMapper.selectByExample(example);

        List<UserDTO> userDTOS = BeanUtil.copyList(users, UserDTO.class);
        if (!CollectionUtils.isEmpty(userDTOS)) {
            //为了减少数据库访问次数，这里先把所有公司和部门读取出来，然后再来匹配
            // 构建用户-公司
            List<Company> companies = companyMapper.selectAll();
            Map<Long, CompanyDTO> companyVOMap = new HashMap<>();
            for (Company company : companies) {
                companyVOMap.put(company.getId(), BeanUtil.copy(company, CompanyDTO.class));
            }

            //构建用户-部门
            List<Department> departments = departmentMapper.selectAll();
            Map<Long, DepartmentDTO> departmentDTOMap = new HashMap<>();
            for (Department department : departments) {
                departmentDTOMap.put(department.getId(), BeanUtil.copy(department, DepartmentDTO.class));
            }

            for (UserDTO userDTO : userDTOS) {
                if (!ObjectUtils.isEmpty(userDTO.getCompanyId())) {
                    userDTO.setCompany(companyVOMap.get(userDTO.getCompanyId()));
                }

                if (!ObjectUtils.isEmpty(userDTO.getDepartmentId())) {
                    userDTO.setDepartment(departmentDTOMap.get(userDTO.getDepartmentId()));
                }
            }
        }
        // 转为 CommonPage返回
        return cast2CommonPage(users, userDTOS);
    }
    @Override
    public RoleDTO queryUserResource(@NotNull Long id) {
        /**
         * 取得用户所有的角色id
         */
        Role role = roleMapper.selectByPrimaryKey(id);
        Example example = new Example(RoleResource.class);
        example.selectProperties("resourceId");
        example.createCriteria().andEqualTo(ROLE_ID, id);
        List<RoleResource> roleResources = roleResourceMapper.selectByExample(example);
        Set<Long> idSet = new HashSet<>();
        for (RoleResource roleResource : roleResources) {
            idSet.add(roleResource.getResourceId());
        }
        // 根据所有的角色id取得所有的资源
        if (!idSet.isEmpty()) {
            Example re = new Example(Resource.class);
            re.createCriteria().andIn("id", idSet);
            // 重复的要去除
            List<Resource> resources = resourceMapper.selectByExample(re);
            resources=resources.stream().distinct().collect(Collectors.toList());
            List<ResourceDTO> resourceDTOS = BeanUtil.copyList(resources, ResourceDTO.class);
            RoleDTO copy = BeanUtil.copy(role, RoleDTO.class);
            copy.setResourceDTOS(resourceDTOS);
            return copy;
        } else {
            return new RoleDTO();
        }
    }

    /***********************************************************************************
     * 以下代码 2023-04-20重构 角色管理模块代码 增加 缓存更新 优化了批量插入的效率
     */
    /**
     *  删除角色的所有的资源
     * @param roleIds  角色id列表
     * @return true成功 false失败  如果成功外部调用删除缓存
     */
    private  boolean removeRolesResources(List<Long> roleIds){
        // 进行角色分配 采用先删除后插入的策略(不删缓存的原因是因为同一个角色在再次分配资源后缓存的key是相同的，因此直接覆盖就好)
        Example example = new Example(RoleResource.class);
        example.createCriteria().andIn(ROLE_ID,roleIds);
        return roleResourceMapper.deleteByExample(example) > 0;
    }

    /**
     * @Description 批量插入角色资源关系
     * @author Gayrot
     * @date 2023/6/12 16:40
     * @param roleIds
     * @param resources
     * @return boolean
     */
    private boolean batchInsertRolesResources(List<Long> roleIds,List<Long> resources){

        if(resources.isEmpty()){
            return true;
        }

        //前端传过来的值目前有问题，先去重
        roleIds = roleIds.stream().distinct().collect(Collectors.toList());
        resources = resources.stream().distinct().collect(Collectors.toList());

        List<RoleResource> roleResourceList = new ArrayList<>();
        // 构建插入数据
        for (Long roleId : roleIds){
            for (Long resourceId : resources) {
                RoleResource roleResource = new RoleResource();
                roleResource.setRoleId(roleId);
                roleResource.setId(idGenerator.snowflakeId());
                roleResource.setResourceId(resourceId);
                super.baseDataStuff4Add(roleResource);
                super.baseDataStuffCommonPros(roleResource);
                roleResourceList.add(roleResource);
            }
        }
        // 批量插入
        return roleResourceMapper.batchInsert(roleResourceList) > 0;
    }

}