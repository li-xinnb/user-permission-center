/**
 * Copyright (C), 2001-2023, www.bosssof.com.cn
 * FileName: ApplicationCache
 * Author: Administrator
 * Date: 2023-04-20 14:03:21
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2023-04-20 14:03:21
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.sd365.permission.centre.service.cache;

import com.alibaba.fastjson.JSON;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.BizErrorCode;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.ResourceMapper;
import com.sd365.permission.centre.dao.mapper.RoleMapper;
import com.sd365.permission.centre.dao.mapper.RoleResourceMapper;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.dto.ResourceDTO;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import lombok.extern.slf4j.Slf4j;
import org.omg.PortableInterceptor.ACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ApplicationCache
 * @Description: 应用程序的缓存统一在这里管理
 * @Author: Administrator
 * @Date: 2023-04-20 14:03
 **/
@Component
@Slf4j
public class ApplicationCache {


    private static final byte ROW_DATA_ACTIVE=1;

    private static final byte ROW_DATA_INACTIVE=0;




}
