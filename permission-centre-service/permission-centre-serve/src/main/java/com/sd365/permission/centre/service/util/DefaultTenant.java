package com.sd365.permission.centre.service.util;
import com.sd365.common.core.common.constant.EntityConsts;
import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.Calendar;
/**
 * TenantBase默认字段填充实体类
 * @Author wujiandong
 * @Date 2022/08/31
 * @Version 1.0.9.1
 **/
@Component
@RefreshScope
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DefaultTenant {

    @Value("${tenant.default.userName}")
    private String userName;

    @Value("${tenant.default.userId}")
    private Long userId;

    @Value("${tenant.default.tenantId}")
    private Long tenantId;

    @Value("${tenant.default.orgId}")
    private Long orgId;

    @Value("${tenant.default.companyId}")
    private Long companyId;

    public void fillField(Object obj) {

        if (obj != null) {
            if (obj instanceof TenantBaseEntity) {
                TenantBaseEntity baseEntity = (TenantBaseEntity) obj;
                baseEntity.setTenantId(tenantId);
                baseEntity.setCompanyId(companyId);
                baseEntity.setOrgId(orgId);
                baseEntity.setCreator(userName);
                baseEntity.setCreatedBy(userId);
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setModifier(userName);
                baseEntity.setUpdatedBy(userId);
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
                baseEntity.setStatus(EntityConsts.INITIAL_STATUS);
                baseEntity.setVersion(EntityConsts.INITIAL_VERSION);
            }
            if (obj instanceof TenantBaseDTO) {
                TenantBaseDTO baseDTO = (TenantBaseDTO) obj;
                baseDTO.setTenantId(tenantId);
                baseDTO.setCompanyId(companyId);
                baseDTO.setOrgId(orgId);
                baseDTO.setCreator(userName);
                baseDTO.setCreatedBy(userId);
                baseDTO.setCreatedTime(Calendar.getInstance().getTime());
                baseDTO.setModifier(userName);
                baseDTO.setUpdatedBy(userId);
                baseDTO.setUpdatedTime(Calendar.getInstance().getTime());
                baseDTO.setStatus(EntityConsts.INITIAL_STATUS);
                baseDTO.setVersion(EntityConsts.INITIAL_VERSION);
            }
            if (obj instanceof TenantBaseVO) {
                TenantBaseVO baseVO = (TenantBaseVO) obj;
                baseVO.setTenantId(tenantId);
                baseVO.setCompanyId(companyId);
                baseVO.setOrgId(orgId);
                baseVO.setCreator(userName);
                baseVO.setCreatedBy(userId);
                baseVO.setCreatedTime(Calendar.getInstance().getTime());
                baseVO.setModifier(userName);
                baseVO.setUpdatedBy(userId);
                baseVO.setUpdatedTime(Calendar.getInstance().getTime());
                baseVO.setStatus(EntityConsts.INITIAL_STATUS);
                baseVO.setVersion(EntityConsts.INITIAL_VERSION);
            }
        }
    }
}
