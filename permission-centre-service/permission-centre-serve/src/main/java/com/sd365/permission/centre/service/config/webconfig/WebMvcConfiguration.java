package com.sd365.permission.centre.service.config.webconfig;

import com.sd365.permission.centre.service.config.interceptor.LogoutInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Description:
 * @Author: WengYu
 * @CreateTime: 2022/06/27 21:45
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

    @Autowired
    private LogoutInterceptor logoutInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(logoutInterceptor).addPathPatterns("/permission/centre/vue-admin-template/user/logout");
    }
}
