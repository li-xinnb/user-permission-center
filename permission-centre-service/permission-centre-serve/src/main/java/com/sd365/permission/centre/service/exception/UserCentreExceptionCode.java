package com.sd365.permission.centre.service.exception;

import com.sd365.common.core.common.exception.code.IErrorCode;
/**
 * @Class UserCentreExceptionCode
 * @Description 根据系统的业务规则定义了业务类
 * @Author Administrator
 * @Date 2023-02-13  11:17
 * @version 1.0.0
 */
public enum UserCentreExceptionCode implements IErrorCode {

    /**
     * 用户操作异常
     */
    BUSINESS_BASEDATA_USER_DB_EXCEPTION_CODE("230501","用户管理模块数据库异常"),
    BUSSINESS_TEL_HAS_EXISTED("90099","该电话号码已存在！！"),
    BUSINESS_BASEDATA_USER_FOUND_EXCEPTION_CODE("230502","用户角色管理模块方法查到对象为空异常"),
    BUSINESS_RECODE_VERSION_NOT_SAME_EXCEPTION("230503","数据版本不一致，无法对该数据进行修改，请刷新数据后再重试"),
    BUSSINESS_RECORD_VERSION_ERROR_OR_ROLE_NOT_EXIST("230555","数据版本不一致或者该角色不存在，请刷新数据后再重试"),
    BUSINESS_ROLE_CODE_HAS_EXIST("230504","该编号已经存在，请换个编号再试  错误码:230504"),
    BUSINESS_ROLE_IS_USING("230500","该角色正在启用，删除失败  错误码:230500"),
    BUSINESS_ROLE_ID_IS_EMPTY("230999","角色id为空，删除失败    错误码:230999"),
    BUSINESS_USER_CENTRE_ROLE_DB_EXCEPTION_CODE("210706", "角色模块数据库异常"),
    RESOURCE_ASSIGN_FAILED("220804","资源分配失败，请检查该角色是否已经拥有该资源"),
    BUSINESS_USER_CENTRE_ROLE_RESOURCE_DB_EXCEPTION_CODE("210707", "角色资源模块数据库异常"),
    BUSINESS_PC_MONITOR_STATE_DB_EXCEPTION_CODE("210704", "数据库外键依赖异常"),
    UNKNOWN_ERROR("2000999","由于未知错误，分配失败"),
    UNKNOWN_DELETE_ERROR("20001000","由于未知错误，删除失败"),
    UNKNOWN_UPDATE_ERROR("20001001","由于未知错误，更新失败"),
    ROLE_NOT_EXIST("20001008","待删除的角色不存在   错误码:20001008"),

    USER_HAS_NOT_LOGIN("990099","用户还未登录，退出登陆失败"),

    USER_PASSWORD_ERROR("990098","原密码输入错误！"),

    HAS_LIFE_SON_DEPARTMENT("250250250","所选部门下有启用的子部门，修改失败"),
    /**
     * company操作
     */
    BUSINESS_COMPANY_HAS_ROLE_CODE("310101", "所选某公司下存在角色，删除失败"),
    BUSINESS_COMPANY_REMOVE_EXCEPTION_CODE("310102", "公司批量删除异常"),
    BUSINESS_COMPANY_REMOVE_BATCH_EXCEPTION_CODE("310103", "公司批量删除异常"),
    /**
     * 租户操作异常
     */
    BUSINESS_HAS_USERS_EXCEPTION_CODE("410100","该租户下存在用户，不能删除！！！"),

    /**
     * message操作异常
     */
    BUSINESS_BASEDATA_MESSAGE_DB_EXCEPTION_CODE("510001","消息管理操作异常");

    /**
     * 错误码
     */
    private String code;
    /**
     * 错误消息
     */
    private String message;

    UserCentreExceptionCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setCode(String s) {
        this.code=code;
    }

    @Override
    public void setMessage(String s) {
        this.message=s;
    }
}
