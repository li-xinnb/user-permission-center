/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName LoginService.java
 * @Author Administrator
 * @Date 2022-10-12  17:06
 * @Description abel.zhan 重构了部分方法增加了注释
 * History:
 * <author> Administrator
 * <time> 2022-10-12  17:06
 * <version> 1.0.0
 * <desc> abel.zhan 2022-10-12 重构 service 和 congroller
 * <br>具体描述:当前版本的问题在于 service设计的时候功能不够内聚导致成为一个贫血的service
 * <br>而使得LoginController的逻辑过去，因此对该类结果做重构，新增 auth 方法 整合相关调用
 */
package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.vo.LoginUserVO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.service.impl.tempEntity.VerificationObject;
import lombok.Data;
import java.util.List;


/**
 * @Description 实现登录功能
 * @author Gayrot
 * @date 2023/6/16 19:26
 */
public interface LoginService {
    /**
     * 错误的租户
     */
    int LOGIN_VERIFY_CODE_TENANT_ERR = 0;
    /**
     * 认证成功 账号 密码 租户一致
     */
    int LOGIN_VERIFY_CODE_SUCCESS = 1;
    /**
     *  账号或者密码错误
     */
    int LOGIN_VERIFY_CODE_CODE_OR_PASSWORD_ERR = 2;

    //用户被停用
    int USER_HAD_BE_STOP = -1;

    //失败码
    int FAIL_CODE = 401;

    //成功码
    int SUCCESS_CODE = 200;

    @Data
    class LoginUserDTO{
        /**
         * 带回认证的错误码
         */
        private int resultCode;
        private Long id;
        private Long tenantId;
        private Long orgId;
        private Long companyId;
        private String name;
        private String tel;
        private List<Long> roleIds;

    }

    /**
     * @Description 身份认证服务
     * @author Gayrot
     * @date 2023/6/16 19:41
     * @param code
     * @param password
     * @param account
     * @return LoginUserVO
     */
    LoginUserVO authHandler(String code, String password, String account);


    /**
     * @Description  登录服务
     * @author Gayrot
     * @date 2023/6/16 19:42
     * @param code
     * @param password
     * @param account
     * @return LoginUserVO
     */
    @Deprecated
    LoginUserVO loginHandler(String code, String password, String account);

    /**
     * @Description  前端认证完成后调用此方法获取用户信息，用户信息包含权限信息
     * @author Gayrot
     * @date 2023/6/16 19:49
     * @param code 工号
     * @param account  租户账号
     * @return LoginUserVO
     */
    LoginUserVO getUserInfoHandler(String code, String account);


    /**
     * @Description 构建用户资源树，用于前端组建侧面菜单栏
     * @author Gayrot
     * @date 2023/6/18 11:22
     * @param id
     * @return List<ResourceVO>
     */
    List<ResourceVO> getUserResourceTreeHandler(Long id);

    /**
     * @Description 退出登录
     * @author Gayrot
     * @date 2023/6/16 19:58
     * @return LoginUserVO
     */
    LoginUserVO logoutHandler();


    /**
     * @Description  账号密码校验
     * 账号(code)和租户(account)不匹配，即 "该账户不存在"，返回0
     * 账号和租户存在数据库且匹配，以及密码正确，则登录成功，返回1
     * 账号和租户匹配，但密码错误，返回2
     * @author Gayrot
     * @date 2023/6/16 19:46
     * @param code
     * @param password
     * @param account
     * @return int 0:账号错误  1:登录成功  2:密码错误
     */
    VerificationObject verification(String code, String password, String account);


    /**
     * @Description 根据工号和租户拿到一个用户
     * @author Gayrot
     * @date 2023/6/16 20:00
     * @param code 工号
     * @param account 租户账号
     * @return User 返回用户 如果没找到返回 null
     */
    User getUserByCodeAndAccount(String code, String account);


    /**
     * @Description 根据用户id查找对应角色列表
     * @author Gayrot
     * @date 2023/6/16 20:01
     * @param userId
     * @return List<Role> 取得用户的所有的角色 如果没有授权返回 null
     */
    List<Role> getRolesByUserId(Long userId);


    /**
     * @Description 取得角色对应的资源树，内部调用
     * @author Gayrot
     * @date 2023/6/16 20:02
     * @param roles 当前用户的所有的角色
     * @return List<ResourceVO> 所拥有的资源 如果角色没有资源方法返回 null
     */
    List<ResourceVO> getResourceVO(List<Role> roles);


    /**
     * @Description 用户登陆成功修改用户状态
     * @author Gayrot
     * @date 2023/6/18 10:49
     * @param userId
     * @param status 1 正常用户 0 注销用户 2 锁定用户 3 在线用户
     * @return Boolean
     */
    @Deprecated
    Boolean updateUserOnlineStatus(Long userId,byte status);


    /**
     * @Description 内部调用 认证和更新用户状态
     * @author Gayrot
     * @date 2023/6/18 10:50
     * @param code
     * @param password
     * @param account
     * @return LoginUserDTO LoginUserDTO.resultCode=0 和 2 代表失败，1 代表成功 成功情况下包含其他信息
     */
    LoginUserDTO auth(String code, String password, String account);


    /**
     * @Description 获取角色的资源列表
     * @author Gayrot
     * @date 2023/6/18 10:50
     * @param roleIdArrays
     * @return List<ResourceVO>
     */
    List<ResourceVO> getResourceVO(Long[] roleIdArrays);
}
