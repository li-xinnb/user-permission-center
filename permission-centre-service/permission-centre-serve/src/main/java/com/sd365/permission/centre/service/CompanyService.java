package com.sd365.permission.centre.service;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.entity.Company;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.query.CompanyQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @Class CompanyService
 * @Description 公司服务接口 主要实现公司的增删改查
 * @Author Administrator
 * @Date 2023-02-20  22:50
 * @version 1.0.0
 */
public interface CompanyService {
    /**
     * 查询公司执行分页
     * @param companyQuery
     * @return
     */
    @Pagination
    List<Company> commonQuery(CompanyQuery companyQuery);

    /**
     * 增加公司
     * @param companyDTO
     * @return true成功 false失败
     */
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    Boolean add(@RequestBody @Valid CompanyDTO companyDTO);
    /**
     * 根据id删除指定的公司
     * @param id
     * @param version
     * @return true成功 false失败
     */
    Boolean remove(Long id, Long version);

    /**
     *  批量删除公司 一个失败事任回滚
     * @param idVersionQueryList
     * @return true成功 false失败
     */
    @Transactional
    Boolean removeBatch(List<IdVersionQuery> idVersionQueryList);
    /**
     *  修改指定的一个公司
     * @param companyDTO
     * @return true成功 false失败
     */
    Boolean modify(CompanyDTO companyDTO);

    /**
     *  根据条件修改公司
     * @param companyDTO
     * @return true成功 false失败
     */
    Boolean batchModify(CompanyDTO companyDTO);

    /**
     *  根据id找到公司
     * @param id 公司记录id
     * @return  公司对象
     */
    CompanyDTO queryById(Long id);

    /**
     *  返回将被拷贝的公司
     * @param id  公司记录id
     * @return 公司对象
     */
    CompanyDTO copy(Long id);
}
