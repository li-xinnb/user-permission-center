package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Message;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.MessageDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.MessageQuery;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface MessageService {

    /**
     * @param messageDTO
     * @return 成功则true
     */
    Boolean add(@RequestBody @Valid MessageDTO messageDTO);

    /**
     * @param messageQuery
     * @return
     */
    List<Message> commonQuery(MessageQuery messageQuery);

    /**
     * @param id
     * @param version
     * @return
     */
    Boolean remove(Long id, Long version);

    /**
     * @param messageDTO
     * @return
     */
    Boolean modify(MessageDTO messageDTO);

    /**
     * @param messageDTOS
     * @return
     */
    @Transactional
    Boolean batchDelete(MessageDTO[] messageDTOS);

    @Transactional
    Boolean batchUpdate(MessageDTO[] messageDTOS);
}
