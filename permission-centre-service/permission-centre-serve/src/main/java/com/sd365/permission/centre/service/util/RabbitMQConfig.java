package com.sd365.permission.centre.service.util;

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
<<<<<<< HEAD
 * @Auther: zkj
 * @Date: 2020/12/10/ 21:57
 * @Description: RabbitMQ配置类
=======
 * @Author:
 * @Description:
 * @Date: Created in 18:16 2020/12/9
 * @Modified By:
>>>>>>> origin/dev
 */
@Configuration
public class RabbitMQConfig {

    @Autowired
    private CachingConnectionFactory connectionFactory;
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public RabbitTemplate rabbitTemplate() {

        RabbitTemplate template = new RabbitTemplate(connectionFactory);

        return template;
    }
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public MQCallbackUtil callbackService() {
        return  new MQCallbackUtil();
    }
}
