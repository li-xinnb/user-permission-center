package com.sd365.permission.centre.service;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.permission.centre.pojo.dto.ResourceDTO;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;

import java.util.List;
//@CacheConfig(cacheNames = "ResourcesService")
/**
 * @Class ResourceService
 * @Description
 * 资源管理接口包括资源的增删改查，注意增对资源操作时需要调用ResourceCache更新redis
 * 资源服务在每日凌晨1点30都被调用 loadResource2Cache 完成资源的与初始化
 * @Author Administrator
 * @Date 2023-05-25  8:48
 * @version 1.0.0
 */
public interface ResourceService {
    /**
     *  将资源初始化redis加快鉴权模块的鉴权速度
     * @param resourceQuery
     * @return 返回初始化的资源列表
     */
    List<ResourceDTO> loadResourceCache(ResourceQuery resourceQuery);

    /**
     * @Description  载入公共资源
     * @author Gayrot
     * @date 2023/6/19 10:48
     */
    void loadCommonResources();
    /**
     * @param: 模糊查询条件
     * 1. 节点名称 name
     * 2. 父亲节点 parentId
     * @return: SubSystemDTOs
     */
    @Pagination
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    CommonPage<ResourceVO> commonQuery(ResourceQuery resourceQuery);

    /**
     * 查询资源树调用
     * @param resourceQuery
     * @return 资源列表
     */
    List<ResourceDTO> query(ResourceQuery resourceQuery);

    /**
     *  增加资源同步redis缓存
     * @param resourceDTO
     * @return 返回增加成功的对象
     */
    //@CachePut(key = "#p0.id")
    ResourceVO add (ResourceDTO resourceDTO);

    /**
     *  删除资源同步redis缓存
     * @param id
     * @return true成功 false失败
     */
    //@CacheEvict(key = "#p0.id")
    Boolean remove(Long id);

    /**
     * 批量删除同步reddis缓存 @TODO 需要补充处理
     * @param resourceDTOS 只需要id和version
     * @return true成功失败false
     */
    Boolean batchDelete(ResourceDTO[] resourceDTOS);

    /**
     * 修改资源同步redis缓存
     * @param resourceDTO
     * @return 插入成功的对象
     */
    //@CachePut(key = "#p0.id")
    ResourceVO modify(ResourceDTO resourceDTO);

    /**
     *  copy等同查找到对象，下一步前端会通过AddDialog 增加一个修改后的数据
     * @TODO 该方法业务应用错误需要重写具体为当前copy插入资源不合理，需要用户修改后插入
     * @param id  记录id
     * @return 返回查找到对象
     */
    ResourceDTO copy(Long id);

    /**
     * 根据id查找资源对象信息
     * @param id  资源id
     * @return 整个资源对象包含父
     */
    ResourceDTO queryById(Long id);
    /**
     * 通过父亲查到到相应的资源对象,该方法应用错误被启用
     * @param id  记录id
     * @return 资源对象
     */
    @Deprecated
    ResourceDTO findByParentId(Long id);

    /**
     * 查找当前租户的所有的资源对象列表，因为命名不规范该方法弃用，
     * @return 资源列表
     */
    @Deprecated
    List<ResourceDTO> queryResourceByMenu();
}
