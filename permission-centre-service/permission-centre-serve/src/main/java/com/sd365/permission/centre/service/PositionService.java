package com.sd365.permission.centre.service;


import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.pojo.dto.DeletePositionDTO;
import com.sd365.permission.centre.pojo.dto.PositionDTO;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @Description 维护职位信息的服务接口
 * @author Gayrot
 * @date 2024/4/1 10:26
 */
public interface PositionService {

    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:26
     * @param postionDTO
     * @return Boolean
     */
    @Transactional
    Boolean add(@RequestBody @Valid PositionDTO postionDTO);


    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:26
     * @param id
     * @param version
     * @return Boolean
     */
    @Transactional
    Boolean remove(Long id, Long version);


    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:27
     * @param positionDTO
     * @return Boolean
     */
    @Transactional
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    Boolean modify(PositionDTO positionDTO);


   /**
    * @Description
    * @author Gayrot
    * @date 2024/4/1 10:27
    * @param postionQuery
    * @return List<PositionDTO>
    */
   List<PositionDTO> commonQuery(PositionQuery postionQuery);

    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:27
     * @param id
     * @return PositionDTO
     */
    PositionDTO queryById(Long id);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */


    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:27
     * @param postionDTOS
     * @return Boolean
     */
    @Transactional
    Boolean batchUpdate(PositionDTO[] postionDTOS);


    /**
     * @Description
     * @author Gayrot
     * @date 2024/4/1 10:27
     * @param deletePositionDTOS
     * @return Boolean
     */
    @Transactional
    Boolean batchDelete(DeletePositionDTO[] deletePositionDTOS);


}
