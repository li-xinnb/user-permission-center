package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Tenant;
import com.sd365.permission.centre.pojo.dto.SubSystemDTO;
import com.sd365.permission.centre.pojo.query.SubSystemQuery;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface SubSystemService {


    /**
     * @param: 子系统DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    Boolean add(@RequestBody @Valid SubSystemDTO subSystemDTO);


    /**
     * @param: 当前行id和当前行版本
     * @return: 成功则true
     * @see
     * @since
     */
    Boolean remove(Long id, Long version);

    /**
     * @param: 子系统DTO数组
     * @return: 成功则true
     * @see
     * @since
     */
    Boolean batchDelete(SubSystemDTO[] subSystemDTOS);


    /**
     * @param: 子系统DTO
     * @return: 成功则true
     * @see
     * @since
     */
    SubSystemDTO modify(SubSystemDTO subSystemDTO);

    /**
     * @param: 模糊查询条件
     * @return: SubSystemDTOs
     * @see
     * @since
     */
    List<SubSystemDTO> commonQuery(SubSystemQuery subSystemQuery);

    /**
     * @param: 子系统id
     * @return: CustomerOrderDTO
     * @see: 查询子系统通过ID
     * @since
     */
    SubSystemDTO querySubSystemById(Long id);

    List<SubSystemDTO> querySubSystemByTenantId(Long id);

    List<Tenant> tenantQuery();
}
