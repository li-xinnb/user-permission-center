package com.sd365.permission.centre.service.impl.tempEntity;

import com.sd365.permission.centre.entity.User;
import lombok.Data;

/**
 * @author Gayrot
 * @Description 身份校验返回类
 * @date 2024/03/29 17:13
 **/
@Data
public class VerificationObject{
    private User user;
    private Integer code;

    public VerificationObject(User user, Integer code) {
        this.user = user;
        this.code = code;
    }
}
