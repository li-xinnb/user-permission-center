package com.sd365.permission.centre.service;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.api.CommonPage;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.pojo.dto.*;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.RoleCompanyVO;
import com.sd365.permission.centre.pojo.vo.RoleVO;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description 优化代码和修复bug，包含了redis初始化角色对应资源（应用于鉴权服务）以及role增删改查方法
 * @author Gayrot
 * @Class RoleService
 * @date 2023/4/12 11:26
 */

public interface RoleService {
    /**
     *  状态标记
     */
    byte INITIALIZE_STATUS_VALUE=1;
    /**
     * 版本标记
     */
    long INITIALIZE_VERSION_VALUE=1;


    /**
     * @Description 把角色-资源载入
     * @author Gayrot
     * @date 2023/4/15 10:07
     * @param role
     * @return Boolean
     */
    Boolean loadRoleResourceCache(@NotNull Role role);



    /**
     * @Description 批量插入，插入前先删除原来的角色和资源的关系
     * @author Gayrot
     * @date 2024/4/12 16:24
     * @param roleResourceDTO
     * @return Boolean 成功ture 失败 false
     */
    @Transactional
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    Boolean assignResource(@NotNull @Valid RoleResourceDTO roleResourceDTO);


    /**
     * @Description 为角色分配用户
     * @author Gayrot
     * @date 2024/4/12 16:42
     * @param userRoleDTO 勾选的用户列表
     * @return Boolean 成功true 否则失败
     */
    @Transactional
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    Boolean assignUser(@NotNull @Valid UserRoleDTO userRoleDTO);

    /**
     * 查询角色所拥有的所有资源
     * @return Node 角色所拥有的资源，这里构建为node而不是resource是比较奇怪的做法
     * 可能是开发人员考虑树的节点上存在公司，资源等等所以统一采用node
     */
    List<Node> queryResourceByRoleId(@NotNull long roleId);

    /**
     * 查询公司节点
     * @return Node 公司节点信息
     */
    @Deprecated
    List<Node> selectCompany();

    /**
     * 验证是否已经存在角色
     * @return true成功 false失败
     */
    boolean haveRole(@NotNull @Valid RoleDTO roleDTO);

    /**
     * @Description 添加角色，角色编号不能重复
     * @author Gayrot
     * @date 2024/4/12 16:05
     * @param roleDTO
     * @return Boolean
     */
    @CommonFieldStuffer(methodType = MethodTypeEnum.ADD)
    Boolean add(@NotNull @Valid RoleDTO roleDTO);


    /**
     * @Description 删除角色
     * @author Gayrot
     * @date 2024/4/13 11:11
     * @param id 角色id
     * @param version 版本
     * @return Boolean true成功 false失败，如果存在关联则抛出异常
     */
    Boolean remove(@NotNull Long id, @NotNull Long version);

    /**
     * @Description 更改角色信息
     * @author Gayrot
     * @date 2024/4/12 15:02
     * @param roleDTO
     * @return RoleDTO
     */
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    RoleDTO modify(@NotNull @Valid RoleDTO roleDTO);

    /**
     * @Description 角色通用分页查询
     * @author Gayrot
     * @date 2024/4/12 16:16
     * @param roleQuery
     * @return CommonPage<RoleDTO>
     */
    @Pagination
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    CommonPage<RoleDTO> commonQuery(@NotNull  RoleQuery roleQuery);


    /**
     * @Description 根据id找到对应的角色
     * @author Gayrot
     * @date 2024/4/15 9:47
     * @param id
     * @return RoleVO
     */
    RoleVO queryById(@NotNull Long id);

    /**
     * @Description 复制一条和RoleDTO 一样的记录并且将值返回前端
     * @author Gayrot
     * @date 2024/4/12 16:11
     * @param id 选中的角色id
     * @return RoleDTO 角色对象
     */
    RoleDTO getRoleDtoForCopy(@NotNull Long id);

    /**
     * @Description 批量删除角色
     * @author Gayrot
     * @date 2024/4/12 14:33
     * @param roleDTOS
     * @return Boolean
     */
    @Transactional
    Boolean batchRemove(@NotEmpty @Valid RoleDTO[] roleDTOS);


    /**
     * @Description 角色界面绑定角色到相应的用户时显示用户列表
     * @author Gayrot
     * @date 2024/4/15 9:18
     * @param userQuery
     * @return CommonPage<UserDTO>
     */
    @Pagination
    CommonPage<UserDTO> commonQueryUserForRoleBindingUsers(@NotNull UserQuery userQuery);

    /**
     *  查询某一个用户所有用的角色
     * @param id
     * @return
     */
    RoleDTO queryUserResource(@NotNull Long id);

}
