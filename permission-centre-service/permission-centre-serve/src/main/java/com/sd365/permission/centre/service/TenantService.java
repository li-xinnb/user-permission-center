package com.sd365.permission.centre.service;

import com.sd365.common.core.annotation.mybatis.Pagination;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;
/**
 * @Class TenantService
 * @Description 租户服务类，是平台多租户的应用的基础
 * 租户的创建需要完成租户管理管理员创建和授权的工作，租户管理员可以进入系统设定和录入租户的数据
 * @Author Administrator
 * @Date 2023-03-15  10:40
 * @version 1.0.0
 */
public interface TenantService {
    /**
     * 增加租户，其中包括完成租户管理的授权
     * @param tenantDTO  租户对象
     * @return 成功则true
     */
    Boolean add(@RequestBody @Valid TenantDTO tenantDTO);

    /**
     * UI 界面支持多条件查询租户,新的分页注解内部包含了 查询结果到Page对象的转换
     * 代码更加简洁
     * @param tenantQuery 查询参数
     * @return 分页的租户列表
     */

    List<TenantDTO> commonQuery(TenantQuery tenantQuery);

    /**
     * 移除租户数据，注意将删除相关授权
     * @param id  租户记录id
     * @param version  记录版本
     * @return  true成功 false失败
     */
    Boolean remove(Long id,Long version);

    /**
     *  修改租户的基础信息，仅仅是租户表的数据不包括关联数据
     * @param tenantDTO  租户对象
     * @return true成功 false失败
     */
    Boolean modify(TenantDTO tenantDTO);

    Boolean updateStatus(TenantDTO tenantDTO);

    /**
     *  UI功能勾选多行数据删除，在一个事务中执行
     * @param deleteTenantDTOS 多行数据 主要去元素的 id和versioin
     * @return true成功 false失败
     */
    @Transactional
    Boolean batchDelete(DeleteTenantDTO[] deleteTenantDTOS);

    /**
     *  根据id查询租户对象，前端加载记录的信息可能用到
     * @param id
     * @return
     */
    TenantDTO queryById(Long id);

    /**
     * 根据名字查询租户信息
     * @param name 租户名字 一般是不重复
     * @return  true 成功 false失败
     */
    TenantDTO queryByName(String name);
}
