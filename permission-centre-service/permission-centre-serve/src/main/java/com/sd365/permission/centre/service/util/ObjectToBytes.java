package com.sd365.permission.centre.service.util;

import java.io.*;

/**
 * @Author: XiongZhiCong
 * @Description: 对象转换成字节的工具类
 * @Date: Created in 10:10 2020/12/8
 * @Modified By:
 */
public class ObjectToBytes {

    /**
     * 对象转化为字节码
     * @param obj
     * @return
     * @throws Exception
     */
    public static   byte[] getBytesFromObject(Serializable obj) throws IOException {
        if (obj == null) {
            return null;
        }
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        ObjectOutputStream oo = new ObjectOutputStream(bo);
        oo.writeObject(obj);
        return bo.toByteArray();
    }

    /**
     * 字节码转化为对象
     * @param objBytes
     * @return
     * @throws Exception
     */
    public static   Object getObjectFromBytes(byte[] objBytes) throws IOException, ClassNotFoundException {
        if (objBytes == null || objBytes.length == 0) {
            return null;
        }
        ByteArrayInputStream bi = new ByteArrayInputStream(objBytes);
        ObjectInputStream oi = new ObjectInputStream(bi);
        return oi.readObject();
    }
}
