package com.sd365.permission.centre.service.impl;

import cn.hutool.http.HttpStatus;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.permission.centre.dao.mapper.ResourceMapper;
import com.sd365.permission.centre.dao.mapper.RoleMapper;
import com.sd365.permission.centre.dao.mapper.TenantMapper;
import com.sd365.permission.centre.dao.mapper.UserMapper;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.CompanyDTO;
import com.sd365.permission.centre.pojo.response.LoginResponseData;
import com.sd365.permission.centre.pojo.vo.LoginUserVO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import com.sd365.permission.centre.service.CompanyService;
import com.sd365.permission.centre.service.LoginService;
import com.sd365.permission.centre.service.MenuService;
import com.sd365.permission.centre.service.UserOnlineService;
import com.sd365.permission.centre.service.exception.UserCentreExceptionCode;
import com.sd365.permission.centre.service.impl.tempEntity.VerificationObject;
import com.sd365.permission.centre.service.util.CoverChineseToEnglish;
import com.sd365.permission.centre.service.util.Md5Utils;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Class LoginServiceImpl
 * @Description  实现登录认证和用户信息获取功能
 * @Author JiaoBingjie
 * @Date 2020/12/11  17:07
 * @version 1.0.0
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {

    /**
     * 用于操作user表验证账号密码
     */
    @Autowired
    private UserMapper userMapper;
    /**
     *  查询租户相关信息
     */
    @Autowired
    private TenantMapper tenantMapper;
    /**
     * 查询角色相关信息
     */
    @Autowired
    private RoleMapper roleMapper;
    /**
     * 查询资源相关信息针对resource表
     */
    @Autowired
    private ResourceMapper resourceMapper;
    /**
     * 主要用于构建菜单
     */
    @Autowired
    private MenuService menuService;

    /**
     *  引入做登录状态更新和登录进入插入
     */
    @Autowired
    private UserOnlineService userOnlineService;

    /**
     * 公司服务
     */
    @Autowired
    private CompanyService companyService;

    /**
     * @Description 用户认证服务
     * @author Gayrot
     * @date 2023/6/16 19:12
     * @param code
     * @param password
     * @param account
     * @return LoginUserVO
     */
    @Override
    public LoginUserVO authHandler(String code, String password, String account) {
        log.info("开始身份认证.........");
        /**
         * 1 认证通过返回带权限的LoginUserVO
         * 2 插入登录记录并且更新登录状态
         */
        LoginUserVO userVO = new LoginUserVO();

        //身份认证
        LoginService.LoginUserDTO loginUserDTO = auth(code,password,account);
        LoginResponseData responseData = new LoginResponseData();
        switch (loginUserDTO.getResultCode())  {
            case LoginService.LOGIN_VERIFY_CODE_TENANT_ERR:
                responseData.setToken("");
                userVO.setData(responseData);
                userVO.setCode(HttpStatus.HTTP_UNAUTHORIZED);
                userVO.setMsg("账号和租户不匹配或不存在");
                break;
            case LoginService.USER_HAD_BE_STOP:
                responseData.setToken("");
                userVO.setData(responseData);
                userVO.setCode(HttpStatus.HTTP_UNAUTHORIZED);
                userVO.setMsg("该用户已被停用，登录失败，请联系管理员！");
                break;
            case LoginService.LOGIN_VERIFY_CODE_SUCCESS:
                // token 在认证服务生成
                responseData.setToken("");
                responseData.setName(CoverChineseToEnglish.cover(loginUserDTO.getName()));
                userVO.setName(CoverChineseToEnglish.cover(loginUserDTO.getName()));
                userVO.setId(loginUserDTO.getId());
                userVO.setData(responseData);
                userVO.setRoleIds(loginUserDTO.getRoleIds());
                userVO.setTenantId(loginUserDTO.getTenantId());
                userVO.setOrgId(loginUserDTO.getOrgId());
                userVO.setCompanyId(loginUserDTO.getCompanyId());
                userVO.setCode(HttpStatus.HTTP_OK);
                userVO.setMsg("登录成功");
                break;
            case LoginService.LOGIN_VERIFY_CODE_CODE_OR_PASSWORD_ERR:
                responseData.setToken("");
                userVO.setData(responseData);
                userVO.setCode(HttpStatus.HTTP_UNAUTHORIZED);
                userVO.setMsg("用户名或密码错误，登录失败");
                break;
            default:
        }
        return userVO;
    }

    @Override
    public LoginUserVO loginHandler(String code, String password, String account) {
        // 根据user.code, user.password, tenant.account
        LoginUserVO userVO = new LoginUserVO();

        userVO.data = new LoginResponseData();

        //用户名，密码，租户匹配
        VerificationObject verificationObject = verification(code, Md5Utils.digestHex(password), account);
        User userInfo = verificationObject.getUser();
        Integer flag = verificationObject.getCode();

        if (flag == LOGIN_VERIFY_CODE_SUCCESS) {

            userVO.data.setToken("1");
            userVO.setCode(SUCCESS_CODE);
            userVO.setMsg("登录成功");
            userVO.setId(userInfo.getId());
            userVO.setCompanyId(userInfo.getCompanyId());
            userVO.setTenantId(userInfo.getTenantId());
            userVO.setOrgId(userInfo.getOrgId());

            //设置用户信息到服务端
            BaseContextHolder.set("user_id",userInfo.getId());
            BaseContextHolder.set("user_name",userInfo.getName());
            BaseContextHolder.set("tenantId",userInfo.getTenantId());
            BaseContextHolder.set("orgId",userInfo.getOrgId());
            BaseContextHolder.set("companyId",userInfo.getCompanyId());

        } else if (flag == LOGIN_VERIFY_CODE_TENANT_ERR) {
            userVO.setCode(FAIL_CODE);
            userVO.setMsg("账号和租户不匹配或不存在");
        }else if(flag == USER_HAD_BE_STOP){
            userVO.setCode(FAIL_CODE);
            userVO.setMsg("该用户已被停用，登录失败，请联系管理员！");
        }
        else {
            userVO.setCode(FAIL_CODE);
            userVO.setMsg("用户名或密码错误，登录失败");
        }
        return userVO;
    }

    @Override
    public LoginUserVO getUserInfoHandler(String code, String account) {
        // 查找对应的用户
        User user = getUserByCodeAndAccount(code, account);
        if(user == null ){
            return null;
        }

        // 查找用户对应的角色
        List<Role> roles = getRolesByUserId(user.getId());

        //过滤被停用的角色
        roles = roles.stream().filter(r->r.getStatus() == 1).collect(Collectors.toList());

        List<String> rolesName = new ArrayList<>();
        List<Long> roleIds = new ArrayList<>();
        if(roles != null) {
            for (Role role : roles) {
                rolesName.add(role.getName());
                roleIds.add(role.getId());
            }
        }
        LoginUserVO userVO = new LoginUserVO();
        userVO.data = new LoginResponseData();
        if(!roles.isEmpty()){
            // 查找对应的资源ResourceVO
            List<ResourceVO> resourceVOS = getResourceVO(roles);
            userVO.data.setResourceVO(resourceVOS);
            log.debug("##resourceVOS:" + resourceVOS);
        }
        userVO.data.setAvatar(user.getProfilePicture());
        userVO.data.setRoles(rolesName);
        userVO.data.setName(user.getName());
        userVO.setCode(HttpStatus.HTTP_OK);
        userVO.setTel(user.getTel());
        userVO.setTenantId(user.getTenantId());
        userVO.setCompanyId(user.getCompanyId());
        userVO.setOrgId(user.getOrgId());
        userVO.setId(user.getId());
        userVO.setRoleIds(roleIds);
        userVO.setMsg("登录成功");
        User userInfo = getUserByCodeAndAccount(code, account);
        userVO.setId(userInfo.getId());
        CompanyDTO companyDTO = companyService.queryById(user.getCompanyId());
        userVO.setCompanyAddress(companyDTO == null ? companyDTO.getAddress() : "您的默认单位地址");
        return userVO;

    }

    @Override
    public List<ResourceVO> getUserResourceTreeHandler(Long id) {
        List<Role> roles = getRolesByUserId(id);
        //获取用户的资源树，用于构建侧边栏
        return getResourceVO(roles);
    }

    @Override
    public LoginUserVO logoutHandler() {
        Long logoutId = BaseContextHolder.getLoginUserId();
        if(logoutId == null){
            throw new BusinessException(UserCentreExceptionCode.USER_HAS_NOT_LOGIN,new Exception("登出失败"));
        }
        userOnlineService.forceLogout(String.valueOf(logoutId));
        LoginUserVO userVO = new LoginUserVO();
        userVO.data = new LoginResponseData();
        userVO.setCode(HttpStatus.HTTP_OK);
        return userVO;
    }

    @Override
    public VerificationObject verification(String code, String password, String account) {
        User user = userMapper.selectNameByCodePassword(code,password);
        if(null != user){
            //用户被停用
            if(user.getStatus() == 0){
                return new VerificationObject(user,USER_HAD_BE_STOP);
            }
            // 可能存在租户输入错误的情况
            Long tenantId = tenantMapper.selectTenantIdByTenantAccount(account);
            if(tenantId == null || !tenantId.equals(user.getTenantId())){
                return new VerificationObject(user,LOGIN_VERIFY_CODE_TENANT_ERR);
            }else{
                return new VerificationObject(user,LOGIN_VERIFY_CODE_SUCCESS);
            }
        }else{
            // 账号或者或者密码已经错误
            return new VerificationObject(user,LOGIN_VERIFY_CODE_CODE_OR_PASSWORD_ERR);
        }
    }

    @Override
    public User getUserByCodeAndAccount(String code, String account) {
        Long tenantId = tenantMapper.selectTenantIdByTenantAccount(account);
        return userMapper.selectUserByCodeTenantid(code, tenantId);
    }

    @Override
    public List<Role> getRolesByUserId(Long userId) {
        return roleMapper.selectRolesByUserId(userId);
    }

    @Override
    public List<ResourceVO> getResourceVO(List<Role> roles) {
        /**
         * 根据角色取得所有的角色id加入列表
         */
        List<Long> roleIds = new ArrayList<>();
        for (Role role : roles) {
            roleIds.add(role.getId());
        }

        // 批量查询取得角色所包含的所有的资源
        List<Resource> resources = resourceMapper.getResourceByRoleId(roleIds);

        // 递归构建资源树
        return menuService.buildMenu(resources);
    }

    @Override
    public Boolean updateUserOnlineStatus(Long userId,byte status) {
        if (userId == null){
            return false;
        }
        return userMapper.updateUserOnlineStatus(userId,status) > 0;
    }


    @Override
    public LoginUserDTO auth(String code, String password, String account) {

        log.info("身份认证中..........");

        LoginUserDTO loginUserDTO = new LoginUserDTO();

        //账号密码租户校验
        VerificationObject verificationObject = verification(code, Md5Utils.digestHex(password), account);
        int result = verificationObject.getCode();
        User user = verificationObject.getUser();
        if (LoginServiceImpl.LOGIN_VERIFY_CODE_SUCCESS == result) {//认证成功

            List<Long> roleIds = new ArrayList<>();
            List<Role> roles = getRolesByUserId(user.getId());

            //拿到角色id列表
            for (Role role : roles) {
                roleIds.add(role.getId());
            }

            loginUserDTO.setId(user.getId());
            loginUserDTO.setName(user.getName());
            loginUserDTO.setRoleIds(roleIds);
            loginUserDTO.setTenantId(user.getTenantId());
            loginUserDTO.setOrgId(user.getOrgId());
            loginUserDTO.setCompanyId(user.getCompanyId());
            loginUserDTO.setResultCode(result);

            //设置用户信息到服务端
            BaseContextHolder.set("user_id",user.getId());
            BaseContextHolder.set("user_name",user.getName());
            BaseContextHolder.set("userName",user.getName());
            BaseContextHolder.set("tenantId",user.getTenantId());
            BaseContextHolder.set("orgId",user.getOrgId());
            BaseContextHolder.set("companyId",user.getCompanyId());
            // 用户登陆判断，1 正常用户 0 注销用户 2 锁定用户 3 在线用户
            if (user.getStatus().intValue() == 1 && user.getStatus().intValue() != 3) {
                // 登陆成功修改status值
                updateUserOnlineStatus(user.getId(), (byte) 3);
            }
            // 保存登陆信息
            userOnlineService.saveLoginInfo(user);
        } else {//认证失败
            loginUserDTO.setResultCode(result);
        }
        return loginUserDTO;
    }

    @Override
    public List<ResourceVO> getResourceVO(Long[] roledArrays) {

        // 批量查询取得角色所包含的所有的资源
        List<Resource> resources = resourceMapper.getResourceByRoleId(Arrays.asList(roledArrays));

        return menuService.buildMenu(resources);
    }
}
