package com.sd365.permission.centre.service.config.interceptor;

import com.sd365.common.core.context.BaseContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description:
 * @Author: WengYu
 * @CreateTime: 2022/06/27 21:41
 */
@Component
@Slf4j
public class LogoutInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisTemplate redisTemplate;

    private static final String USER_LOGING_KEY = "user:loginInfo:";
    private static final String USER_TOKEN_KEY = "user:token:";

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String logoutId = BaseContextHolder.get("logoutId");
        redisTemplate.delete(USER_TOKEN_KEY + logoutId);
        redisTemplate.delete(USER_LOGING_KEY + logoutId);
    }
}
