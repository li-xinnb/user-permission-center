package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.entity.Position;
import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UpdatePasswordDTO;
import com.sd365.permission.centre.pojo.dto.UserCentreDTO;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import com.sd365.permission.centre.pojo.dto.pcp.PcpUserDTO;
import com.sd365.permission.centre.pojo.dto.shop.ShopBuyerDTO;
import com.sd365.permission.centre.pojo.dto.UserSelfInfoDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Description
 * @author Gayrot
 * @date 2024/3/11 19:04
 */
public interface UserService {

    @Transactional
    Boolean add(@Valid @RequestBody UserDTO userDTO);

    @Transactional
    Boolean remove(Long id, Long version);


    @Transactional
    Boolean modify(UserDTO userDTO);


    Boolean modifyUserInfo(UserCentreDTO userCentreDTO);


    @Transactional
    Boolean modifyUserInfoForBm(UserCentreDTO userCentreDTO);


    @Transactional
    Boolean modifyWithNewRole(UserDTO[] userDTOS);



    List<User> commonQuery(UserQuery userQuery);


    User queryById(Long id);


    List<Role> queryAllRole();

    List<Department> queryAllDepartment(@NotNull DepartmentQuery departmentQuery);

    List<Position> queryAllPosition(PositionQuery positionQuery);

    @Transactional
    Boolean batchUpdate(UserDTO[] userDTOS);

    @Transactional
    Boolean batchDelete(UserDTO[] userDTOS);

    Boolean firstStartMd5();

    @Transactional
    Boolean pcpUserRegister(List<PcpUserDTO> pcpUserDTOS);

    @Transactional
    Boolean pcpUserUpdate(List<PcpUserDTO> pcpUserDTOS);

    @Transactional
    Boolean updateUserLockStatus(List<PcpUserDTO> pcpUserDTOS);

    Boolean updateUserLockStatus(PcpUserDTO userDTO);

    @Deprecated
    @Transactional
    Boolean shopUserRegister(List<ShopBuyerDTO> shopBuyerDTOS);

    Boolean updatePassword(UserDTO userDTO);

    User getUser(String code );

    Boolean updateMyselfInfo(@Valid UserSelfInfoDTO userDTO);


    Boolean updatePassword(@Valid UpdatePasswordDTO userDTO);
}
