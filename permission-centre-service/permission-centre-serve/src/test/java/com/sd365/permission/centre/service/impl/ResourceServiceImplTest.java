package com.sd365.permission.centre.service.impl;

import com.sd365.App;
import com.sd365.permission.centre.pojo.dto.ResourceDTO;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import com.sd365.permission.centre.service.ResourceService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
//@SpringBootTest(classes = {App.class},webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@SpringBootTest(classes = {App.class})
//@EnableCaching
@Slf4j
class ResourceServiceImplTest {
    /**
     * 验证redis对象使用
     */
    @Autowired
    private RedisTemplate<String,Object> redisTemplate;
    /**
     * 被测试对象
     */
    @Autowired
    private ResourceService resourceService;
    private Long id = new Long(1);

//    private ResourceQuery resourceQuery= BeanUtil.copy(resourceService.queryById(id),ResourceQuery.class);

    @Test
    Boolean add() {
        ResourceDTO resourceDTO = new ResourceDTO();
        resourceDTO.setCreatedBy(111L);
        resourceService.add(resourceDTO);
        return true;
    }

    @Test
    void batchDelete() {
    }

    @Test
    void modify() {
        ResourceDTO resource = new ResourceDTO();
        resource.setId(new Long(1));
        resource.setVersion(new Long(1));
        resource.setUpdatedTime(new Date());
        resourceService.modify(resource);
    }

    @Test
    void copy() {
    }

    @Test
    void testloadResource2Cache(){
       List<ResourceDTO> resourceDTOList= resourceService.loadResourceCache(new ResourceQuery());
//       resourceDTOList.stream().forEach(resourceDTO -> {
//          Object resource= redisTemplate.opsForValue().get(Global.RESOURCE_ID_KEY+resourceDTO.getId());
//          Assert.assertEquals(JSON.parseObject(JSONObject.toJSONString(resource),Resource.class) .getName(),resourceDTO.getName());
//       });
    }



}